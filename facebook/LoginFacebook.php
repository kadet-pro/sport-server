<?php
// Include the autoloader provided in the SDK
require_once $_SERVER['DOCUMENT_ROOT'] .'/libs/facebook-php-sdk/autoload.php';

// Include required libraries
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

/**
 * Created by PhpStorm.
 * User: Vovnov
 * Date: 23.07.2018
 * Time: 22:12
 */
class LoginFacebook
{
    public static function getUserDataByAccessToken($access_token)
    {
        $fb = new  Facebook([
            'app_id' => '281356712748664',
            'app_secret' => '0a1788c9a0239b9c1ac8279d7ef9d54c',
            'default_graph_version' => 'v3.0',
        ]);

        try {
            $response = $fb->get('/me?fields=name,first_name,last_name,email,link,gender,locale,cover,picture.type(large)', $access_token);
            $response = $response->getGraphNode()->asArray();
        } catch (FacebookResponseException $e) {
            return NULL;
        } catch (FacebookSDKException $e) {
            return NULL;
        }
        return $response;
    }
}