<?php

/**
 * Created by PhpStorm.
 * User: Vovnov
 * Date: 08.02.2019
 * Time: 0:20
 */
class Exercise
{
    var $id;
    var $name = NULL;
    var $description = NULL;
    var $level;
    var $muscles;
    var $concentration;
    var $exerciseImage = array();

    public function getDataForApi()
    {
        $result = array();
        $result["id"] = $this->id;
        $result["name"] = $this->name;
        $result["description"] = $this->description;
        $result["level"] = $this->level;
        $result["muscles"] = $this->muscles;
        $result["images"] = $this->exerciseImage;
        $result["concentration"] = $this->concentration;
        return $result;
    }


    public function parseFromDataBase($row)
    {
        $this->id = $row['ID'];
        $this->name = $row['NAME'];
        $this->description = $row['DESCRIPTION'];
        $this->level = $row['LEVEL'];
        $this->muscles = $row['MUSCLES'];
        $this->concentration = $row['CONCENTRATION'];
    }



    public function isEmpty()
    {
        if ($this->name == NULL
            && $this->description == NULL
        )
            return true;
        else
            return false;
    }

    public function addImages($images)
    {
        $this->exerciseImage = $images;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getConcentration()
    {
        return $this->concentration;
    }

    /**
     * @param mixed $concentration
     */
    public function setConcentration($concentration)
    {
        $this->concentration = $concentration;
    }


}