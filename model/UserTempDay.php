<?php


class UserTempDay
{
    var $id;
    var $user_temp_id;
    var $template_day_id;
    var $day_training = NULL;
    var $start_datetime = NULL;
    var $end_datetime = NULL;
    var $completed = false;
    var $is_rest;
    var $exercise;

    public function getDataForApi()
    {
        $result = array();
        $result["id"] = $this->id;
        $result["user_temp_id"] = $this->user_temp_id;
        $result["template_day_id"] = $this->template_day_id;
        $result["day_training"] = $this->day_training;
        $result["completed"] = $this->completed;
        $result["exercise"] = $this->exercise;
        $result["start_datetime"] = $this->start_datetime;
        $result["end_datetime"] = $this->end_datetime;
        $result["is_rest"] = $this->is_rest;
        return $result;
    }

    public function parseFromDataBase($row)
    {
        $this->id = $row['ID'];
        $this->user_temp_id = $row['USER_TEMP_ID'];
        $this->template_day_id = $row['TEMPLATE_DAY_ID'];
        $this->day_training = $row['DAY_TRAINING'];
        $this->start_datetime = $row['START_DATETIME'];
        $this->end_datetime = $row['END_DATETIME'];
        $this->completed = $row['COMPLETED'] == 1 ? true : false;
        $this->is_rest = $row['IS_REST'] == 1 ? true : false;
    }


    /**
     * @param mixed $sessions
     */
    public function setExercise($exercise)
    {
        $this->exercise = $exercise;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserTempId()
    {
        return $this->user_temp_id;
    }

    /**
     * @param mixed $user_temp_id
     */
    public function setUserTempId($user_temp_id)
    {
        $this->user_temp_id = $user_temp_id;
    }

    /**
     * @return mixed
     */
    public function getTemplateDayId()
    {
        return $this->template_day_id;
    }

    /**
     * @param mixed $template_day_id
     */
    public function setTemplateDayId($template_day_id)
    {
        $this->template_day_id = $template_day_id;
    }

    /**
     * @return null
     */
    public function getDayTraining()
    {
        return $this->day_training;
    }

    /**
     * @param null $day_training
     */
    public function setDayTraining($day_training)
    {
        $this->day_training = $day_training;
    }


    /**
     * @param null $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return bool
     */
    public function isCompleted()
    {
        return $this->completed;
    }

    /**
     * @param bool $completed
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;
    }

}