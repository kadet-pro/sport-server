<?php

/**
 * Created by PhpStorm.
 * User: valer
 * Date: 12/25/2019
 * Time: 4:39 PM
 */
class UserEvent
{
    var $id = 0;
    var $title = "";
    var $description = "";
    var $datetime = "";
    var $notification = false;

    public function parseFromDataBase($row)
    {
        $this->id = $row['ID'];
        $this->title = $row['TITLE'];
        $this->description = $row['DESCRIPTION'];
        $this->datetime = $row['DATETIME'];
        $this->notification = $row['NOTIFICATION'] == 1 ? true : false;
    }

    public function getDataForApi()
    {
        $result = array();
        $result["id"] = $this->id;
        $result["title"] = $this->title;
        $result["description"] = $this->description;
        $result["datetime"] = $this->datetime;
        $result["notification"] = $this->notification ;
        return $result;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @param string $datetime
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    }

    /**
     * @return bool
     */
    public function isNotification()
    {
        return $this->notification;
    }

    /**
     * @param bool $notification
     */
    public function setNotification($notification)
    {
        $this->notification = $notification;
    }


}