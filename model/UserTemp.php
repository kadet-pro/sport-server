<?php


class UserTemp
{
    var $id;
    var $template_id;
    var $status_active = true;
    var $days;

    public function getDataForApi()
    {
        $result = array();
        $result["id"] = $this->id;
        $result["template_id"] = $this->template_id;
        $result["status_active"] = $this->status_active;
        $result["days"] = $this->days;
        $result["o"] = 0;
        return $result;
    }

    public function parseFromDataBase($row)
    {
        $this->id = $row['ID'];
        $this->template_id = $row['TEMPLATE_ID'];
        $this->status_active = $row['STATUS_ACTIVE'] == 1 ? true : false;
    }

    /**
     * @return mixed
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * @param mixed $days
     */
    public function setDays($days)
    {
        $this->days = $days;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTemplateId()
    {
        return $this->template_id;
    }

    /**
     * @param mixed $template_id
     */
    public function setTemplateId($template_id)
    {
        $this->template_id = $template_id;
    }

    /**
     * @return bool
     */
    public function isStatusActive()
    {
        return $this->status_active;
    }

    /**
     * @param bool $status_active
     */
    public function setStatusActive($status_active)
    {
        $this->status_active = $status_active;
    }


    /**
     * @param null $start_date
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
    }



    /**
     * @param null $start_time
     */
    public function setStartTime($start_time)
    {
        $this->start_time = $start_time;
    }


}