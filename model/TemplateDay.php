<?php

/**
 * Created by PhpStorm.
 * User: Vovnov
 * Date: 19.08.2019
 * Time: 21:03
 */
class TemplateDay
{
    var $id;
    var $template_id;
    var $day_in_week;
    var $day_training;
    var $is_rest;
    var $details;

    public function getDataForApi()
    {
        $result = array();
        $result["id"] = $this->id;
        $result["template_id"] = $this->template_id;
        $result["day_in_week"] = $this->day_in_week;
        $result["day_training"] = $this->day_training;
        $result["is_rest"] = $this->is_rest;
        $result["details"] = $this->details;
        $result["o"] = 0;
        return $result;
    }

    public function parseFromDataBase($row)
    {
        $this->id = $row['ID'];
        $this->template_id = $row['TEMPLATE_ID'];
        $this->day_in_week = $row['DAY_IN_WEEK'];
        $this->day_training = $row['DAY_TRAINING'];
        $this->is_rest = $row['IS_REST'] == 1 ? true : false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTemplateId()
    {
        return $this->template_id;
    }

    /**
     * @param mixed $template_id
     */
    public function setTemplateId($template_id)
    {
        $this->template_id = $template_id;
    }

    /**
     * @return mixed
     */
    public function getDayInWeek()
    {
        return $this->day_in_week;
    }

    /**
     * @param mixed $day_in_week
     */
    public function setDayInWeek($day_in_week)
    {
        $this->day_in_week = $day_in_week;
    }

    /**
     * @return mixed
     */
    public function getDayTraining()
    {
        return $this->day_training;
    }

    /**
     * @param mixed $day_training
     */
    public function setDayTraining($day_training)
    {
        $this->day_training = $day_training;
    }

    /**
     * @return mixed
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param mixed $details
     */
    public function setDetails($details)
    {
        $this->details = $details;
    }



}