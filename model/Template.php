<?php

/**
 * Created by PhpStorm.
 * UserDetails: Vovnov
 * Date: 14.10.2018
 * Time: 23:00
 */
class Template
{
    var $id;
    var $name = NULL;
    var $description = NULL;
    var $photo_url;
    var $for_whom;
    var $goal;
    var $training_place;
    var $level_of_training;
    var $training_time;
    var $count_training_per_week;
    var $cycle_time_in_week;
    var $is_favorite;
    var $type_training_template;

    public function getDataForApi()
    {
        $result = array();
        $result["id"] = $this->id;
        $result["name"] = $this->name;
        $result["description"] = $this->description;
        $result["photo_url"] = $this->photo_url;
        $result["for_whom"] = $this->for_whom;
        $result["goal"] = $this->goal;
        $result["training_place"] = $this->training_place;
        $result["level_of_training"] = $this->level_of_training;
        $result["training_time"] = $this->training_time;
        $result["count_training_per_week"] = $this->count_training_per_week;
        $result["cycle_time_in_week"] = $this->cycle_time_in_week;
        $result["is_favorite"] = $this->is_favorite;
        $result["type_training_template"] = $this->type_training_template;
        return $result;
    }

    public function parseFromDataBase($row)
    {
        $this->id = $row['ID'];
        $this->name = $row['NAME'];
        $this->description = $row['DESCRIPTION'];
        $this->photo_url = $row['PHOTO_URL'];
        $this->for_whom = $row['FOR_WHOM'];
        $this->goal = $row['GOAL'];
        $this->training_place = $row['TRAINING_PLACE'];
        $this->level_of_training = $row['LEVEL_OF_TRAINING'];
        $this->training_time = $row['TRAINING_TIME'];
        $this->count_training_per_week = $row['COUNT_TRAINING_PER_WEEK'];
        $this->cycle_time_in_week = $row['CYCLE_TIME_IN_WEEK'];
        $this->type_training_template = $row['TYPE_TRAINING_TEMPLATE'];
    }

    public function parseFromDataBaseWithFavoriteOption($row)
    {
        $this->parseFromDataBase($row);
        if ($row['IS_FAVORITE'] == 1)
            $this->is_favorite = TRUE;
        else
            $this->is_favorite = FALSE;
    }

    public function isEmpty()
    {
        if ($this->name == NULL
            && $this->description == NULL
        )
            return true;
        else
            return false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPhotoUrl()
    {
        return $this->photo_url;
    }

    /**
     * @param mixed $photo_url
     */
    public function setPhotoUrl($photo_url)
    {
        $this->photo_url = $photo_url;
    }

    /**
     * @return mixed
     */
    public function getPlane()
    {
        return $this->plane;
    }

    /**
     * @param mixed $plane
     */
    public function setPlane($plane)
    {
        $this->plane = $plane;
    }

    /**
     * @return mixed
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param mixed $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }
}