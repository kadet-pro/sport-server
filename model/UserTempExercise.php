<?php

/**
 * Created by PhpStorm.
 * User: Vovnov
 * Date: 15.08.2019
 * Time: 23:12
 */
class UserTempExercise
{
    var $id;
    var $user_temp_day_id;
    var $exercise_id;
    var $type_metric;
    var $date_create;
    var $exerciseSet;


    public function getDataForApi()
    {
        $result = array();
        $result["id"] = $this->id;
        $result["user_temp_day_id"] = $this->user_temp_day_id;
        $result["exercise_id"] = $this->exercise_id;
        $result["type_metric"] = $this->type_metric;
        $result["exercise_set"] = $this->exerciseSet;
        $result["date_create"] = $this->date_create;
        return $result;
    }

    public function parseFromDataBase($row)
    {
        $this->id = $row['ID'];
        $this->user_temp_day_id = $row['USER_TEMP_DAY_ID'];
        $this->exercise_id = $row['EXERCISE_ID'];
        $this->type_metric = $row['TYPE_METRIC'];
        $this->date_create = $row['DATE_CREATE'];
    }

    /**
     * @return mixed
     */
    public function getExerciseSet()
    {
        return $this->exerciseSet;
    }

    /**
     * @param mixed $progress
     */
    public function setProgress($exerciseSet)
    {
        $this->exerciseSet = $exerciseSet;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserTempSessionId()
    {
        return $this->user_temp_session_id;
    }

    /**
     * @param mixed $user_temp_session_id
     */
    public function setUserTempSessionId($user_temp_session_id)
    {
        $this->user_temp_session_id = $user_temp_session_id;
    }

    /**
     * @return mixed
     */
    public function getExerciseId()
    {
        return $this->exercise_id;
    }

    /**
     * @param mixed $exercise_id
     */
    public function setExerciseId($exercise_id)
    {
        $this->exercise_id = $exercise_id;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getIsRest()
    {
        return $this->is_rest;
    }

    /**
     * @param mixed $is_rest
     */
    public function setIsRest($is_rest)
    {
        $this->is_rest = $is_rest;
    }

    /**
     * @return mixed
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * @param mixed $date_create
     */
    public function setDateCreate($date_create)
    {
        $this->date_create = $date_create;
    }


}