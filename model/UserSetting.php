<?php

/**
 * Created by PhpStorm.
 * User: Vovnov
 * Date: 16.02.2019
 * Time: 18:39
 */
class UserSetting
{
    var $notification_start_training = true;
    var $notification_txt = "";
    var $notification_time_start_training = "";
    var $training_time_countdown = 3;
    var $training_time_exercise = 30;
    var $training_time_rest = 10;
    var $exercise_random = false;

    public function parseFromDataBase($row)
    {
        $this->notification_start_training = $row['NOTIFICATION_START_TRAINING'] == 1 ? true : false;
        $this->notification_txt = $row['NOTIFICATION_TXT'];
        $this->notification_time_start_training = $row['NOTIFICATION_TIME_START_TRAINING'];
        $this->training_time_countdown = $row['TRAINING_TIME_COUNTDOWN'];
        $this->training_time_exercise = $row['TRAINING_TIME_EXERCISE'];
        $this->training_time_rest = $row['TRAINING_TIME_REST'];
        $this->exercise_random = $row['EXERCISE_RANDOM'] == 1 ? true : false;
    }

    public function getDataForApi()
    {
        $result = array();
        $result["notification_start_training"] = $this->notification_start_training;
        $result["notification_txt"] = $this->notification_txt;
        $result["notification_time_start_training"] = $this->notification_time_start_training;
        $result["training_time_countdown"] = $this->training_time_countdown;
        $result["training_time_exercise"] = $this->training_time_exercise;
        $result["training_time_rest"] = $this->training_time_rest;
        $result["exercise_random"] = $this->exercise_random;

        return $result;
    }

    /**
     * @return bool
     */
    public function isNotificationStartTraining()
    {
        return $this->notification_start_training;
    }

    /**
     * @param bool $notification_start_training
     */
    public function setNotificationStartTraining($notification_start_training)
    {
        $this->notification_start_training = $notification_start_training;
    }

    /**
     * @return string
     */
    public function getNotificationTxt()
    {
        return $this->notification_txt;
    }

    /**
     * @param string $notification_txt
     */
    public function setNotificationTxt($notification_txt)
    {
        $this->notification_txt = $notification_txt;
    }

    /**
     * @return string
     */
    public function getNotificationTimeStartTraining()
    {
        return $this->notification_time_start_training;
    }

    /**
     * @param string $notification_time_start_training
     */
    public function setNotificationTimeStartTraining($notification_time_start_training)
    {
        $this->notification_time_start_training = $notification_time_start_training;
    }

    /**
     * @return int
     */
    public function getTrainingTimeCountdown()
    {
        return $this->training_time_countdown;
    }

    /**
     * @param int $training_time_countdown
     */
    public function setTrainingTimeCountdown($training_time_countdown)
    {
        $this->training_time_countdown = $training_time_countdown;
    }

    /**
     * @return int
     */
    public function getTrainingTimeExercise()
    {
        return $this->training_time_exercise;
    }

    /**
     * @param int $training_time_exercise
     */
    public function setTrainingTimeExercise($training_time_exercise)
    {
        $this->training_time_exercise = $training_time_exercise;
    }

    /**
     * @return int
     */
    public function getTrainingTimeRest()
    {
        return $this->training_time_rest;
    }

    /**
     * @param int $training_time_rest
     */
    public function setTrainingTimeRest($training_time_rest)
    {
        $this->training_time_rest = $training_time_rest;
    }

    /**
     * @return bool
     */
    public function isExerciseRandom()
    {
        return $this->exercise_random;
    }

    /**
     * @param bool $exercise_random
     */
    public function setExerciseRandom($exercise_random)
    {
        $this->exercise_random = $exercise_random;
    }


}