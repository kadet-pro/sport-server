<?php

/**
 * Created by PhpStorm.
 * UserDetails: Vovnov
 * Date: 15.08.2018
 * Time: 21:15
 */
class UserDetails
{
    var $name = '';
    var $photo_url = '';
    var $years = 0;
    var $sex = 0;
    var $weight = 0;
    var $height = 0;
    var $metric_type = 0;
    var $target = 0;

    public function parseFromDataBase($row)
    {
        $this->name = $row['NAME'];
        $this->photo_url = $row['PHOTO_URL'];
        $this->years = $row['YEARS'];
        $this->sex = $row['SEX'];
        $this->weight = $row['WEIGHT'];
        $this->height = $row['HEIGHT'];
        $this->metric_type = $row['METRIC_TYPE'];
        $this->target = $row['TARGET'];
    }

    public function parseFromFacebook($result_facebook)
    {
        if (isset($result_facebook['name']))
            $this->name = $result_facebook['name'];
        if (isset($result_facebook['picture'])) {
            $picture = $result_facebook['picture'];
            if (isset($picture['url']))
                $this->photo_url = $picture['url'];
        }
    }

    public function getDataForApi()
    {
        $result = array();
        $result["name"] = $this->name;
        $result["photo_url"] = $this->photo_url;
        $result["years"] = $this->years;
        $result["sex"] = $this->sex;
        $result["weight"] = $this->weight;
        $result["height"] = $this->height;
        $result["metric_type"] = $this->metric_type;
        $result["target"] = $this->target;

        return $result;
    }

    public function isEmpty()
    {
        if ($this->name == ''
            && $this->photo_url == ''
            && $this->years == 0
            && $this->sex == 0
            && $this->weight == 0
            && $this->height == 0
            && $this->metric_type == 0
            && $this->target == 0
        )
            return true;
        else
            return false;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhotoUrl()
    {
        return $this->photo_url;
    }

    /**
     * @param string $photo_url
     */
    public function setPhotoUrl($photo_url)
    {
        $this->photo_url = $photo_url;
    }

    /**
     * @return int
     */
    public function getYears()
    {
        return $this->years;
    }

    /**
     * @param int $years
     */
    public function setYears($years)
    {
        $this->years = $years;
    }

    /**
     * @return int
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * @param int $sex
     */
    public function setSex($sex)
    {
        $this->sex = $sex;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getMetricType()
    {
        return $this->metric_type;
    }

    /**
     * @param int $metric_type
     */
    public function setMetricType($metric_type)
    {
        $this->metric_type = $metric_type;
    }

    /**
     * @return int
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param int $target
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }


}