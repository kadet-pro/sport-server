<?php

/**
 * Created by PhpStorm.
 * UserDetails: Vovnov
 * Date: 03.02.2019
 * Time: 11:41
 */
class UserConfig
{
    var $id;
    var $email = NULL;
    var $access_token = NULL;
    var $status = 0;

    public function parseFromDataBase($row)
    {
        $this->id = $row['ID'];
        $this->email = $row['EMAIL'];
        $this->access_token = $row['ACCESS_TOKEN'];
        $this->status = $row['STATUS'];
    }

    public function getDataForApi()
    {
        $result = array();
        $result["id"] = $this->id;
        $result["email"] = $this->email;
        $result["access_token"] = $this->access_token;
        $result["status"] = $this->status;
        return $result;
    }

    public function isEmpty()
    {
        if ($this->email == NULL
            && $this->access_token == NULL
        )
            return true;
        else
            return false;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param null $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param null $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return null
     */
    public function getAccessToken()
    {
        return $this->access_token;
    }

    /**
     * @param null $access_token
     */
    public function setAccessToken($access_token)
    {
        $this->access_token = $access_token;
    }


}