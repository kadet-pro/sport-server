<?php


class UserTempExerciseSet
{
    var $id;
    var $user_temp_exercise_id;
    var $time;
    var $mass;
    var $repetitions = 0;
    var $is_rest;
    var $date_create;

    public function getDataForApi()
    {
        $result = array();
        $result["id"] = $this->id;
        $result["user_temp_exercise_id"] = $this->user_temp_exercise_id;
        $result["time"] = $this->time;
        $result["mass"] = $this->mass;
        $result["is_rest"] = $this->is_rest;
        $result["date_create"] = $this->date_create;
        $result["repetitions"] = $this->repetitions;
        return $result;
    }

    public function parseFromDataBase($row)
    {
        $this->id = $row['ID'];
        $this->user_temp_exercise_id = $row['USER_TEMP_EXERCISE_ID'];
        $this->time = $row['TIME'];
        $this->mass = $row['MASS'];
        $this->date_create = $row['DATE_CREATE'];
        $this->repetitions = $row['REPETITIONS'];
        $this->is_rest = $row['IS_REST'] == 1 ? true : false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserTempExerciseId()
    {
        return $this->user_temp_exercise_id;
    }

    /**
     * @param mixed $user_temp_exercise_id
     */
    public function setUserTempExerciseId($user_temp_exercise_id)
    {
        $this->user_temp_exercise_id = $user_temp_exercise_id;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getMass()
    {
        return $this->mass;
    }

    /**
     * @param mixed $mass
     */
    public function setMass($mass)
    {
        $this->mass = $mass;
    }

    /**
     * @return mixed
     */
    public function getDateCreate()
    {
        return $this->date_create;
    }

    /**
     * @param mixed $date_create
     */
    public function setDateCreate($date_create)
    {
        $this->date_create = $date_create;
    }


}