<?php

/**
 * Created by PhpStorm.
 * User: Vovnov
 * Date: 19.08.2019
 * Time: 20:58
 */
class TemplateExercise
{
    var $id;
    var $type_metric;
    var $template_day_id;
    var $exercise_id;
    var $exercise;
    var $sets;

    public function getDataForApi()
    {
        $result = array();
        $result["id"] = $this->id;
        $result["type_metric"] = $this->type_metric;
        $result["template_day_id"] = $this->template_day_id;
        $result["exercise_id"] = $this->exercise_id;
        $result["exercise"] = $this->exercise;
        $result["sets"] = $this->sets;
        $result["o"] = 0;
        return $result;
    }

    public function parseFromDataBase($row)
    {
        $this->id = $row['ID'];
        $this->type_metric = $row['TYPE_METRIC'];
        $this->template_day_id = $row['TEMPLATE_DAY_ID'];
        $this->exercise_id = $row['EXERCISE_ID'];
    }

    /**
     * @return mixed
     */
    public function getSets()
    {
        return $this->sets;
    }

    /**
     * @param mixed $sets
     */
    public function setSets($sets)
    {
        $this->sets = $sets;
    }



    /**
     * @return mixed
     */
    public function getExercise()
    {
        return $this->exercise;
    }

    /**
     * @param mixed $exercise
     */
    public function setExercise($exercise)
    {
        $this->exercise = $exercise;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTypeMetric()
    {
        return $this->type_metric;
    }

    /**
     * @param mixed $type_metric
     */
    public function setTypeMetric($type_metric)
    {
        $this->type_metric = $type_metric;
    }

    /**
     * @return mixed
     */
    public function getTemplateDayId()
    {
        return $this->template_day_id;
    }

    /**
     * @param mixed $template_day_id
     */
    public function setTemplateDayId($template_day_id)
    {
        $this->template_day_id = $template_day_id;
    }

    /**
     * @return mixed
     */
    public function getExerciseId()
    {
        return $this->exercise_id;
    }

    /**
     * @param mixed $exercise_id
     */
    public function setExerciseId($exercise_id)
    {
        $this->exercise_id = $exercise_id;
    }


}