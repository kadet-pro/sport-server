<?php

/**
 * Created by PhpStorm.
 * User: Vovnov
 * Date: 19.08.2019
 * Time: 21:14
 */
class TemplateExerciseSet
{
    var $id = 0;
    var $template_exercise_id = 0;
    var $time = 0;
    var $repetitions = 0;
    var $mass = 0;

    public function getDataForApi()
    {
        $result = array();
        $result["id"] = $this->id;
        $result["template_exercise_id"] = $this->template_exercise_id;
        $result["time"] = $this->time;
        $result["repetitions"] = $this->repetitions;
        $result["mass"] = $this->mass;
        return $result;
    }

    public function parseFromDataBase($row)
    {
        $this->id = $row['ID'];
        $this->template_exercise_id = $row['TEMPLATE_EXERCISE_ID'];
        $this->time = $row['TIME'];
        $this->repetitions = $row['REPETITIONS'];
        $this->mass = $row['MASS'];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTemplateExerciseId()
    {
        return $this->template_exercise_id;
    }

    /**
     * @param mixed $template_exercise_id
     */
    public function setTemplateExerciseId($template_exercise_id)
    {
        $this->template_exercise_id = $template_exercise_id;
    }

    /**
     * @return mixed
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * @param mixed $minutes
     */
    public function setMinutes($minutes)
    {
        $this->minutes = $minutes;
    }

    /**
     * @return mixed
     */
    public function getRepetitions()
    {
        return $this->repetitions;
    }

    /**
     * @param mixed $repetitions
     */
    public function setRepetitions($repetitions)
    {
        $this->repetitions = $repetitions;
    }

    /**
     * @return mixed
     */
    public function getMass()
    {
        return $this->mass;
    }

    /**
     * @param mixed $mass
     */
    public function setMass($mass)
    {
        $this->mass = $mass;
    }


}