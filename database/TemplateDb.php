<?php
require_once dirname(__FILE__) . '/BaseDb.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserConfig.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/Template.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/TemplateExerciseSet.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/TemplateDay.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/TemplateExercise.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserTemp.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserTempDay.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserTempExerciseSet.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/Exercise.php';

/**
 * Created by PhpStorm.
 * User: Vovnov
 * Date: 07.02.2019
 * Time: 20:50
 */
class TemplateDb extends BaseDb
{

    //region template

    public function getBaseTemplate($user_id)
    {
        $stmt = $this->conn->prepare("SELECT A.* , if (B.ID is not null , 1, 0) as IS_FAVORITE 
                                      FROM TEMPLATE A LEFT JOIN USER_TEMPLATE_FAVORITE B ON A.ID = B.TEMPLATE_ID AND B.USER_ID = ? WHERE ACTIVE = 1 ORDER BY FOR_WHOM DESC");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $templates = array();
        $template = new Template();
        while ($row = $res->fetch_assoc()) {
            $template->parseFromDataBaseWithFavoriteOption($row);
            array_push($templates, $template->getDataForApi());
        }
        $stmt->close();
        return $templates;
    }

    public function getSavedTemplate($user_id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM TEMPLATE WHERE ACTIVE = 1 AND ID IN (SELECT TEMPLATE_ID FROM USER_TEMPLATE_FAVORITE WHERE USER_ID = ?)");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $templates = array();
        $template = new Template();
        while ($row = $res->fetch_assoc()) {
            $template->parseFromDataBase($row);
            array_push($templates, $template->getDataForApi());
        }
        $stmt->close();
        return $templates;
    }

    public function getPopularTemplate($user_id)
    {
        $stmt = $this->conn->prepare("SELECT A.*, if (C.ID is not null , 1, 0) as IS_FAVORITE FROM 
                                    (SELECT TEMPLATE_ID, COUNT(TEMPLATE_ID) AS ORDER_TEMP FROM USER_TEMP GROUP BY TEMPLATE_ID) AS B 
                                    LEFT JOIN TEMPLATE A ON A.ID = B.TEMPLATE_ID 
                                    LEFT JOIN USER_TEMPLATE_FAVORITE C ON B.TEMPLATE_ID = C.TEMPLATE_ID AND C.USER_ID = ? 
                                    WHERE A.ACTIVE = 1  
                                    ORDER BY B.ORDER_TEMP");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $templates = array();
        $template = new Template();
        while ($row = $res->fetch_assoc()) {
            $template->parseFromDataBase($row);
            array_push($templates, $template->getDataForApi());
        }
        $stmt->close();
        return $templates;
    }

    public function getForYouTemplate($user_id)
    {
        $user = $this->getUserDetails($user_id);
        $sex = $user->getSex();
        $target = $user->getTarget();
        $stmt = $this->conn->prepare("SELECT A.* , if (B.ID is not null , 1, 0) as IS_FAVORITE 
                                      FROM TEMPLATE A LEFT JOIN USER_TEMPLATE_FAVORITE B ON A.ID = B.TEMPLATE_ID AND B.USER_ID = ? 
                                      WHERE ACTIVE = 1 AND A.FOR_WHOM in (?,3) AND GOAL = ?");
        $stmt->bind_param("iii", $user_id, $sex, $target);
        $stmt->execute();
        $res = $stmt->get_result();
        $templates = array();
        $template = new Template();
        while ($row = $res->fetch_assoc()) {
            $template->parseFromDataBase($row);
            array_push($templates, $template->getDataForApi());
        }
        $stmt->close();
        return $templates;
    }

    public function getBaseTemplateById($template_id)
    {
        $stmt = $this->conn->prepare("SELECT  A.* FROM TEMPLATE A WHERE A.ID = ?");
        $stmt->bind_param("i", $template_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $template = new Template();
        while ($row = $res->fetch_assoc()) {
            $template->parseFromDataBase($row);
        }
        $stmt->close();
        return $template;
    }

    public function getExerciseByTemplateId($template_id)
    {
        $resultFinal = array();
        $stmtDay = $this->conn->prepare("SELECT * FROM TEMPLATE_DAY WHERE TEMPLATE_ID = ?");
        $stmtDay->bind_param("i", $template_id);
        $stmtDay->execute();
        $resDay = $stmtDay->get_result();
        while ($rowDay = $resDay->fetch_assoc()) {
            $resultExercise = array();
            $templateDay = new TemplateDay();
            $templateDay->parseFromDataBase($rowDay);
            $stmt = $this->conn->prepare("SELECT * FROM TEMPLATE_EXERCISE WHERE TEMPLATE_DAY_ID = ?");
            $templateDayId = $templateDay->getId();
            $stmt->bind_param("i", $templateDayId);
            $stmt->execute();
            $res = $stmt->get_result();
            $templateExercise = new TemplateExercise();
            while ($row = $res->fetch_assoc()) {
                $templateExercise->parseFromDataBase($row);
                $templateExercise->setExercise($this->getExerciseById($templateExercise->getExerciseId()));
                $templateExercise->setSets($this->getSetsByTemplateExerciseId($templateExercise->getId()));
                array_push($resultExercise, $templateExercise->getDataForApi());
            }
            $stmt->close();
            $templateDay->setDetails($resultExercise);
            array_push($resultFinal, $templateDay->getDataForApi());
        }
        $resDay->close();
        return $resultFinal;
    }

    private function getSetsByTemplateExerciseId($template_exercise_id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM TEMPLATE_EXERCISE_SET WHERE TEMPLATE_EXERCISE_ID = ?");
        $stmt->bind_param("i", $template_exercise_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $list = array();
        $set = new TemplateExerciseSet();
        while ($row = $res->fetch_assoc()) {
            $set->parseFromDataBase($row);
            array_push($list, $set->getDataForApi());
        }
        $stmt->close();
        return $list;
    }

    private function getExerciseById($exercise_id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM EXERCISE WHERE ID = ?");
        $stmt->bind_param("i", $exercise_id);
        $exercise = new Exercise();
        $stmt->execute();
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $exercise->parseFromDataBase($row);
            $exercise->addImages($this->getExerciseImages($exercise->getId()));
        }
        $stmt->close();
        return $exercise->getDataForApi();

    }

    private function getExerciseImages($exercise_id)
    {
        $stmt = $this->conn->prepare("SELECT PHOTO_URL FROM EXERCISE_IMAGE WHERE EXERCISE_ID = ?");
        $stmt->bind_param("i", $exercise_id);
        $result = array();
        $stmt->execute();
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            array_push($result, $row["PHOTO_URL"]);
        }
        $stmt->close();
        return $result;
    }

    //endregion

    //region start or stop template
    public function addUserFavoriteTemplate($user_id, $template_id)
    {
        $stmt = $this->conn->prepare("CALL ADD_USER_FAVORITE_TEMPLATE(?,?)");
        $stmt->bind_param("ii", $user_id, $template_id);
        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function startTemplate($user_id, $template_id, $start_date)
    {
        $template = $this->getBaseTemplateById($template_id);
        $user_started_template_id = 0;
        if ($template->type_training_template == 2) //days
        {
            $stmt = $this->conn->prepare("SELECT START_TEMPLATE_DAY(?,?,?) as RESULT");
        } else { // 1 week
            $stmt = $this->conn->prepare("SELECT START_TEMPLATE_WEEK(?,?,?) as RESULT");
        }
        $stmt->bind_param("iis", $user_id, $template_id, $start_date);
        $stmt->execute();
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $user_started_template_id = $row['RESULT'];
        }
        $stmt->close();
        return $this->getUserStartedTemplate($user_id, $user_started_template_id);
    }

    public function isUserStartedTemplate($user_id, $template_id)
    {
        $stmt = $this->conn->prepare("SELECT IS_USER_STARTED_TEMPLATE(?,?) as RESULT");
        $stmt->bind_param("ii", $user_id, $template_id);
        $stmt->execute();
        $result = 0;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $result = $row['RESULT'];
        }
        $stmt->close();
        return $result == 0 ? false : true;
    }

    public function isAlreadyStartedSomeTemplate($user_id)
    {
        $stmt = $this->conn->prepare("SELECT IS_ALREADY_STARTED_SOME_TEMPLATE(?) as RESULT");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $result = 0;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $result = $row['RESULT'];
        }
        $stmt->close();
        return $result == 0 ? false : true;
    }

    public function getUserStartedTemplate($user_id, $user_started_template_id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM USER_TEMP WHERE USER_ID = ? AND ID = ?");
        $stmt->bind_param("ii", $user_id, $user_started_template_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $userTemp = new UserTemp();
        while ($row = $res->fetch_assoc()) {
            $userTemp->parseFromDataBase($row);
        }
        $stmt->close();
        $resultDays = array();
        $stmt = $this->conn->prepare("SELECT * FROM USER_TEMP_DAY WHERE USER_TEMP_ID = ?");
        $userTempId = $userTemp->getId();
        $stmt->bind_param("i", $userTempId);
        $stmt->execute();
        $res = $stmt->get_result();
        $day = new UserTempDay();
        while ($row = $res->fetch_assoc()) {
            $day->parseFromDataBase($row);
            $user_temp_day_id = $day->getId();
            $day->setExercise($this->getUserTempExerciseByDayId($user_temp_day_id));
            array_push($resultDays, $day->getDataForApi());
        }
        $stmt->close();
        $userTemp->setDays($resultDays);
        return $userTemp->getDataForApi();
    }

    public function stopTemplate($user_temp_id, $user_id)
    {
        $stmt = $this->conn->prepare("CALL STOP_TEMPLATE(?,?)");
        $stmt->bind_param("ii", $user_temp_id, $user_id);
        $stmt->execute();
        $stmt->close();
    }

    //endregion

    //region statistics by template

    public function getAllStartedTemplates($user_id)
    {
        $resultFinal = array();
        $stmt = $this->conn->prepare("SELECT * FROM USER_TEMP WHERE USER_ID = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $result = array();
        while ($row = $res->fetch_assoc()) {
            $result["template"] = $this->getBaseTemplateById($row['TEMPLATE_ID'])->getDataForApi();
            $result["template_details"] = $this->getExerciseByTemplateId($row['TEMPLATE_ID']);
            $result["started_template"] = $this->getUserStartedTemplate($user_id, $row['ID']);
            array_push($resultFinal, $result);
        }
        $stmt->close();
        return $resultFinal;
    }
    //endregion

    //region add or get user template day

    public function finishTrainingDay($training_events, $start_datetime, $end_datetime, $user_started_template_day_id)
    {
        $this->updFinishTrainingDay($user_started_template_day_id, $start_datetime, $end_datetime);
        foreach ($training_events as &$value) {
            $user_temp_exercise_id = $this->addUserTempExercises($user_started_template_day_id, $value["exercise_id"], $value["type_metric"]);
            $data = $value["exercise_set"];
            foreach ($data as &$value1) {
                $this->addUserTempExercisesSet($user_temp_exercise_id, $value1["time"], $value1["mass"], $value1["is_rest"] ? 1 : 0, $value1["repetitions"]);
            }
        }
        return $this->getUserStartedTemplateDay($user_started_template_day_id);
    }

    public function updFinishTrainingDay($user_started_template_day_id, $start_datetime, $end_datetime)
    {
        $stmt = $this->conn->prepare("CALL FINISH_TRAINING_DAY(?,?,?)");
        $stmt->bind_param("iss", $user_started_template_day_id, $start_datetime, $end_datetime);
        $stmt->execute();
        $stmt->close();
    }

    public function addUserTempExercisesSet($user_template_exercise_id, $time, $mass, $is_rest, $repetitions)
    {
        $stmt = $this->conn->prepare("CALL ADD_USER_TEMP_EXERCISE_SET(?,?,?,?,?)");
        $stmt->bind_param("iiiii", $user_template_exercise_id, $time, $mass, $is_rest,$repetitions);
        $stmt->execute();
        $stmt->close();
    }

    public function addUserTempExercises($user_temp_day_id, $exercise_id, $type_metric)
    {
        $result = 0;
        $stmt = $this->conn->prepare("SELECT ADD_USER_TEMP_EXERCISE(?,?,?) as RESULT");
        $stmt->bind_param("iii", $user_temp_day_id, $exercise_id, $type_metric);
        $stmt->execute();
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $result = $row['RESULT'];
        }
        $stmt->close();
        return $result;
    }

    public function getUserStartedTemplateDay($user_started_template_day_id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM USER_TEMP_DAY WHERE ID = ?");
        $stmt->bind_param("i", $user_started_template_day_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $resultSession = new UserTempDay();
        while ($row = $res->fetch_assoc()) {
            $resultSession->parseFromDataBase($row);
            $resultSession->setExercise($this->getUserTempExerciseByDayId($row["ID"]));
        }
        $stmt->close();
        return $resultSession->getDataForApi();
    }

    public function getUserTempExerciseByDayId($user_temp_day_id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM USER_TEMP_EXERCISE WHERE USER_TEMP_DAY_ID = ?");
        $stmt->bind_param("i", $user_temp_day_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $trainingEvents = new UserTempExercise();
        $resultDays = array();
        while ($row = $res->fetch_assoc()) {
            $trainingEvents->parseFromDataBase($row);
            $trainingEvents->setProgress($this->getUserTempExerciseSet($trainingEvents->getId()));
            array_push($resultDays, $trainingEvents->getDataForApi());
        }
        $stmt->close();
        return $resultDays;
    }

    public function getUserTempExerciseSet($user_temp_exercise_id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM USER_TEMP_EXERCISE_SET WHERE USER_TEMP_EXERCISE_ID = ?");
        $stmt->bind_param("i", $user_temp_exercise_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $trainingEvents = new UserTempExerciseSet();
        $resultDays = array();
        while ($row = $res->fetch_assoc()) {
            $trainingEvents->parseFromDataBase($row);
            array_push($resultDays, $trainingEvents->getDataForApi());
        }
        $stmt->close();
        return $resultDays;
    }

    //endregion
}