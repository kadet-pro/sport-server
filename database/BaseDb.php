<?php
require_once dirname(__DIR__) . '/model/UserConfig.php';
require_once dirname(__DIR__) . '/model/UserSetting.php';
require_once dirname(__DIR__) . '/model/UserDetails.php';

class BaseDb
{
    /**
     * @var conn
     */
    protected $conn;

    function __construct()
    {
        require_once dirname(__FILE__) . '/DbConnect.php';
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    public function getUserSetting($user_id)
    {
        $stmt = $this->conn->prepare("SELECT NOTIFICATION_START_TRAINING, NOTIFICATION_TXT, NOTIFICATION_TIME_START_TRAINING, TRAINING_TIME_COUNTDOWN, TRAINING_TIME_EXERCISE, TRAINING_TIME_REST, EXERCISE_RANDOM FROM USER_SETTING WHERE USER_ID = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $user = new UserSetting();
        while ($row = $res->fetch_assoc()) {
            $user->parseFromDataBase($row);
        }
        $stmt->close();
        return $user;
    }

    public function getUserConfig($user_id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM USER_CONFIG WHERE ID = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $user = new UserConfig();
        while ($row = $res->fetch_assoc()) {
            $user->parseFromDataBase($row);
        }
        $stmt->close();
        return $user;
    }

    public function getUserDetails($user_id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM USER WHERE ID = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $user = new UserDetails();
        while ($row = $res->fetch_assoc()) {
            $user->parseFromDataBase($row);
        }
        $stmt->close();
        return $user;
    }

    public function updateUserDetails($user_id, UserDetails $userDetails)
    {
        $stmt = $this->conn->prepare("CALL UPDATE_USER_DETAILS(?,?,?,?,?,?,?,?,?)");

        $name = $userDetails->getName();
        $photo_url = $userDetails->getPhotoUrl();
        $years = $userDetails->getYears();
        $sex = $userDetails->getSex();
        $weight = $userDetails->getWeight();
        $height = $userDetails->getHeight();
        $metric_type = $userDetails->getMetricType();
        $target = $userDetails->getTarget();

        $stmt->bind_param("issiiiiii",
            $user_id, $name, $photo_url, $years,
            $sex, $weight, $height, $metric_type,
            $target);
        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function saveFilePatch($user_id, $patch)
    {
        $stmt = $this->conn->prepare("CALL SAVE_FILE_PATCH(?,?)");
        $stmt->bind_param("is", $user_id, $patch);
        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function getUserIdByAccessToken($access_token)
    {
        $stmt = $this->conn->prepare("SELECT GET_USER_ID_BY_ACCESS_TOKEN(?) AS RESULT");
        $stmt->bind_param("s", $access_token);
        $stmt->execute();
        $result = 0;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $result = $row['RESULT'];
        }
        $stmt->close();
        return $result;
    }
}

?>
