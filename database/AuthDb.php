<?php
require_once dirname(__FILE__) . '/BaseDb.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserConfig.php';

/**
 * Created by PhpStorm.
 * User: Vovnov
 * Date: 03.02.2019
 * Time: 15:35
 */
class AuthDb extends BaseDb
{
    public function createUser($email, $password, $name)
    {
        $stmt = $this->conn->prepare("SELECT CREATE_USER(?,?,?,?) as RESULT");
        $accessToken = $this->generateAccessToken();
        $stmt->bind_param("ssss", $email, $password, $accessToken, $name);
        $stmt->execute();
        $user_id = 0;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $user_id = $row['RESULT'];
        }
        $stmt->close();
        if ($user_id != 0) {
            $userConfig = new UserConfig();
            $userConfig->setId($user_id);
            $userConfig->setAccessToken($accessToken);
            $userConfig->setEmail($email);
            $userConfig->setName($name);
            return $userConfig;
        } else return NULL;
    }

    public function getUserConfigIfExist($email, $password)
    {
        $stmt = $this->conn->prepare("SELECT GET_USER_ID_IF_EXIST(?,?) as RESULT");
        $stmt->bind_param("ss", $email, $password);
        $stmt->execute();
        $user_id = 0;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $user_id = $row['RESULT'];
        }
        $stmt->close();
        if ($user_id != 0) {
            return $this->getUserConfig($user_id);
        } else
            return NULL;
    }

    public function createUserFromFacebook($email, $password, $name, $facebook_id)
    {
        $stmt = $this->conn->prepare("SELECT CREATE_USER_FACEBOOK(?,?,?,?,?) as RESULT");
        $accessToken = $this->generateAccessToken();
        $stmt->bind_param("sssss", $email, $password, $accessToken, $name, $facebook_id);
        $stmt->execute();
        $user_id = 0;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $user_id = $row['RESULT'];
        }
        $stmt->close();
        if ($user_id != 0) {
            $userConfig = new UserConfig();
            $userConfig->setId($user_id);
            $userConfig->setAccessToken($accessToken);
            $userConfig->setEmail($email);
            $userConfig->setName($name);
            return $userConfig;
        } else return NULL;
    }

    public function getUserConfigByFacebookId($facebook_id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM USER_CONFIG WHERE FACEBOOK_ID = ?");
        $stmt->bind_param("i", $facebook_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $user = new UserConfig();
        while ($row = $res->fetch_assoc()) {
            $user->parseFromDataBase($row);
        }
        $stmt->close();
        return $user;
    }

    public function isFacebookIdExist($facebook_id)
    {
        $stmt = $this->conn->prepare("SELECT IS_FACEBOOK_ID_EXIST(?) as RESULT");
        $stmt->bind_param("s", $facebook_id);
        $stmt->execute();
        $result = 0;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $result = $row['RESULT'];
        }
        $stmt->close();
        return $result == 0 ? false : true;
    }

    public function isEmailExist($email)
    {
        $stmt = $this->conn->prepare("SELECT IS_EMAIL_EXIST(?) as RESULT");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $result = 0;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $result = $row['RESULT'];
        }
        $stmt->close();
        return $result == 0 ? false : true;
    }

    public function updateFbUserDetails($user_id, $name, $photo_url)
    {
        $stmt = $this->conn->prepare("CALL UPDATE_FB_USER_DETAILS(?,?,?)");
        $stmt->bind_param("iss", $user_id, $name, $photo_url);
        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function updateUserDetails($user_id, UserDetails $userDetails)
    {
        $stmt = $this->conn->prepare("CALL COMPLETE_USER_LOGIN(?,?,?,?,?,?,?)");

        $years = $userDetails->getYears();
        $sex = $userDetails->getSex();
        $weight = $userDetails->getWeight();
        $height = $userDetails->getHeight();
        $metricType = $userDetails->getMetricType();
        $target = $userDetails->getTarget();

        $stmt->bind_param("iiiiiii",
            $user_id,  $sex, $years,
            $weight, $height, $metricType,
            $target);
        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    private function generateAccessToken()
    {
        return md5(uniqid(rand(), true));
    }
}