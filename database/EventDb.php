<?php
require_once dirname(__FILE__) . '/BaseDb.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserEvent.php';

/**
 * Created by PhpStorm.
 * User: Vovnov
 * Date: 25.07.2019
 * Time: 22:07
 */
class EventDb extends BaseDb
{
    public function deleteEvent($event_id, $user_id)
    {
        $stmt = $this->conn->prepare("CALL REMOVE_USER_EVENT(?,?)");
        $stmt->bind_param("ii", $event_id, $user_id);
        $stmt->execute();
        $stmt->close();
    }

    public function createOrUpdateEvent($id, $user_id, $title, $description, $datetime, $notification)
    {
        $stmt = $this->conn->prepare("SELECT CREATE_OR_UPDATE_USER_EVENT(?,?,?,?,?,?) AS RESULT");
        $stmt->bind_param("iisssi", $id, $user_id, $title, $description, $datetime, $notification);
        $stmt->execute();
        $result = 0;
        $res = $stmt->get_result();
        while ($row = $res->fetch_assoc()) {
            $result = $row['RESULT'];
        }
        $stmt->close();
        $userEvent = new UserEvent();
        $userEvent->setId($result);
        $userEvent->setTitle($title);
        $userEvent->setDescription($description);
        $userEvent->setDatetime($datetime);
        $userEvent->setNotification($notification == 1 ? true : false);
        return $userEvent->getDataForApi();
    }

    public function getAllUserEvents($user_id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM USER_EVENT WHERE USER_ID = ?");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $res = $stmt->get_result();
        $events = array();
        $userEvent = new UserEvent();
        while ($row = $res->fetch_assoc()) {
            $userEvent->parseFromDataBase($row);
            array_push($events, $userEvent->getDataForApi());
        }
        $stmt->close();
        return $events;
    }
}