<?php
require_once dirname(__FILE__) . '/BaseDb.php';

class JobDB extends BaseDb
{
    public function checkToCompleteUserTemplate()
    {
        $stmt = $this->conn->prepare("CALL JOB_CHECK_TO_COMPLETE_USER_TEMPLATE()");
        $stmt->execute();
        $stmt->close();
    }
}