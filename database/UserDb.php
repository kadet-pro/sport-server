<?php

require_once dirname(__FILE__) . '/BaseDb.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserConfig.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserSetting.php';

/**
 * Created by PhpStorm.
 * User: Vovnov
 * Date: 03.02.2019
 * Time: 17:37
 */
class UserDb extends BaseDb
{
    public function updateUserNameFacebook($user_id, $name)
    {
        $stmt = $this->conn->prepare("CALL UPDATE_USER_NAME_FACEBOOK(?,?)");
        $stmt->bind_param("is", $user_id, $name);
        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function updateUserName($user_id, $name)
    {
        $stmt = $this->conn->prepare("CALL UPDATE_USER_NAME(?,?)");
        $stmt->bind_param("is", $user_id, $name);
        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function updateUserSetting($user_id, UserSetting $userSetting)
    {
        $notification_start_training = $userSetting->isNotificationStartTraining() ? 1 : 0;
        $notification_txt = $userSetting->getNotificationTxt();
        $notification_time_start_training = $userSetting->getNotificationTimeStartTraining();
        $training_time_countdown = $userSetting->getTrainingTimeCountdown();
        $training_time_exercise = $userSetting->getTrainingTimeExercise();
        $training_time_rest = $userSetting->getTrainingTimeRest();
        $exercise_random = $userSetting->isExerciseRandom() ? 1 : 0;
        $stmt = $this->conn->prepare("CALL UPDATE_USER_SETTING(?,?,?,?,?,?,?,?)");
        $stmt->bind_param("iissiiii", $user_id, $notification_start_training, $notification_txt, $notification_time_start_training,
            $training_time_countdown, $training_time_exercise, $training_time_rest, $exercise_random);
        $stmt->execute();
        $stmt->close();
    }
}