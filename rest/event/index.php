<?php
header('Content-Type: text/html; charset=UTF-8');

require_once $_SERVER['DOCUMENT_ROOT'] . '/database/EventDb.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/Config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/utils/ErrorCode.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserDetails.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserConfig.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/Template.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/utils/BaseResponse.php';
require $_SERVER['DOCUMENT_ROOT'] . '/libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$user_id = NULL;

function authenticate(\Slim\Route $route)
{
    $headers = apache_request_headers();
    $app = \Slim\Slim::getInstance();
    if (isset($headers['x-sport-access-token'])) {
        $db = new BaseDb();
        $token = $headers['x-sport-access-token'];
        $g_user_id = $db->getUserIdByAccessToken($token);
        if ($g_user_id == 0) {
            BaseResponse::echoBadResponse("Invalidate access token", 401, ErrorCode::INVALIDATE_ACCESS_TOKEN, $app);
            $app->stop();
        } else {
            global $user_id;
            $user_id = $g_user_id;
        }
    } else {
        BaseResponse::echoBadResponse("Did not find access token", 400, ErrorCode::ERROR_READING_PARAMETERS, $app);
        $app->stop();
    }
}

$app->post('/create_ot_update', 'authenticate', function () use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    try {
        $id = $data["id"];
        $title = $data["title"];
        $description = $data["description"];
        $datetime = $data["datetime"];
        $notification = $data["notification"] ? 1 : 0;
    } catch (Exception $ex) {
        $id = null;
        $title = null;
        $description = null;
        $datetime = null;
        $notification = null;
    }

    if ($title != null && $description != null && $datetime != null) {
        $db = new EventDb();
        global $user_id;
        $data = $db->createOrUpdateEvent($id, $user_id, $title, $description, $datetime, $notification);
        $response = array();
        $response["event"] = $data;
        BaseResponse::echoGoodResponse("Event handled", $response, 201, $app);
    } else
        BaseResponse::echoBadResponse("Attributes not found", 200, ErrorCode::ERROR_READING_PARAMETERS, $app);
});


$app->post('/delete_event', 'authenticate', function () use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    try {
        $event_id = $data["event_id"];
    } catch (Exception $ex) {
        $event_id = 0;
    }
    if ($event_id != 0) {
        $db = new EventDb();
        global $user_id;
        $db->deleteEvent($event_id, $user_id);
        BaseResponse::echoGoodResponse("Event deleted", NULL, 200, $app);
    } else
        BaseResponse::echoBadResponse("Attributes not found", 200, ErrorCode::ERROR_READING_PARAMETERS, $app);
});

$app->get('/all_events', 'authenticate', function () use ($app) {
    global $user_id;
    $db = new EventDb();
    $data = $db->getAllUserEvents($user_id);
    $response = array();
    $response["events"] = $data;
    BaseResponse::echoGoodResponse("User events", $response, 200, $app);
});

function getValueFromRequest($key, $default)
{
    $app = \Slim\Slim::getInstance();
    try {
        return $app->request()->get($key);
    } catch (Exception $e) {
        return $default;
    }
}

$app->run();
?>