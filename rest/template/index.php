<?php
header('Content-Type: text/html; charset=UTF-8');

require_once $_SERVER['DOCUMENT_ROOT'] . '/database/TemplateDb.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/Config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/utils/ErrorCode.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserDetails.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserConfig.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/Template.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserTempDay.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserTempExerciseSet.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserTempExercise.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserTemp.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/utils/BaseResponse.php';
require $_SERVER['DOCUMENT_ROOT'] . '/libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$user_id = NULL;

function authenticate(\Slim\Route $route)
{
    $headers = apache_request_headers();
    $app = \Slim\Slim::getInstance();
    if (isset($headers['x-sport-access-token'])) {
        $db = new BaseDb();
        $token = $headers['x-sport-access-token'];
        $g_user_id = $db->getUserIdByAccessToken($token);
        if ($g_user_id == 0) {
            BaseResponse::echoBadResponse("Invalidate access token", 401, ErrorCode::INVALIDATE_ACCESS_TOKEN, $app);
            $app->stop();
        } else {
            global $user_id;
            $user_id = $g_user_id;
        }
    } else {
        BaseResponse::echoBadResponse("Did not find access token", 400, ErrorCode::ERROR_READING_PARAMETERS, $app);
        $app->stop();
    }
}

$app->get('/home', 'authenticate', function () use ($app) {
    global $user_id;
    $db = new TemplateDb();
    $response = array();
    $response["base"] = $db->getBaseTemplate($user_id);
    $response["favorite"] = $db->getSavedTemplate($user_id);
    $response["popular"] = $db->getPopularTemplate($user_id);
    $response["for_you"] = $db->getForYouTemplate($user_id);
    BaseResponse::echoGoodResponse("base templates", $response, 200, $app);
});

$app->get('/base', 'authenticate', function () use ($app) {
    global $user_id;
    $db = new TemplateDb();
    $data = $db->getBaseTemplate($user_id);
    $response = array();
    $response["base"] = $data;
    BaseResponse::echoGoodResponse("base templates", $response, 200, $app);
});

$app->get('/favorite', 'authenticate', function () use ($app) {
    global $user_id;
    $db = new TemplateDb();
    $data = $db->getSavedTemplate($user_id);
    $response = array();
    $response["favorite"] = $data;
    BaseResponse::echoGoodResponse("favorite templates", $response, 200, $app);
});

$app->post('/add_favorite', 'authenticate', function () use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    try {
        $template_id = $data["template_id"];
    } catch (Exception $ex) {
        $template_id = 0;
    }
    if ($template_id != 0) {
        $db = new TemplateDb();
        global $user_id;
        $db->addUserFavoriteTemplate($user_id, $template_id);
        BaseResponse::echoGoodResponse("Template handled", NULL, 201, $app);
    } else
        BaseResponse::echoBadResponse("Attributes not found", 200, ErrorCode::ERROR_READING_PARAMETERS, $app);
});

$app->get('/details', 'authenticate', function () use ($app) {
    $template_id = getValueFromRequest('template_id', NULL);
    if (!empty($template_id)) {
        $db = new TemplateDb();
        $response = array();
        $response["days"] = $db->getExerciseByTemplateId($template_id);
        BaseResponse::echoGoodResponse("details templates", $response, 200, $app);
    } else BaseResponse::echoBadResponse("Attributes not found", 200, ErrorCode::ERROR_READING_PARAMETERS, $app);
});

$app->post('/start_template', 'authenticate', function () use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    try {
        $template_id = $data["template_id"];
        $start_date = $data["start_date"];
    } catch (Exception $ex) {
        $template_id = 0;
        $start_date = NULL;
    }
    if ($template_id != 0 && $start_date != NULL) {
        $db = new TemplateDb();
        global $user_id;
        if ($db->isAlreadyStartedSomeTemplate($user_id)) {
            BaseResponse::echoBadResponse("You already started some template", 200,
                ErrorCode::ERROR_ALREADY_STARTED_SOME_TEMPLATE, $app);
            return;
        }
        if ($db->isUserStartedTemplate($user_id, $template_id)) {
            BaseResponse::echoBadResponse("This template already started", 200,
                ErrorCode::ERROR_TEMPLATE_ALREADY_STARTED, $app);
            return;
        }
        $data = $db->startTemplate($user_id, $template_id, $start_date);
        $response = array();
        $response["started_template"] = $data;
        BaseResponse::echoGoodResponse("Started template", $response, 200, $app);
    } else
        BaseResponse::echoBadResponse("Attributes not found", 200, ErrorCode::ERROR_READING_PARAMETERS, $app);
});

$app->post('/stop_template', 'authenticate', function () use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    try {
        $user_temp_id = $data["user_temp_id"];
    } catch (Exception $ex) {
        $user_temp_id = 0;
    }
    if ($user_temp_id != 0) {
        $db = new TemplateDb();
        global $user_id;
        $db->stopTemplate($user_temp_id, $user_id);
        BaseResponse::echoGoodResponse("Stopped template", null, 200, $app);
    } else
        BaseResponse::echoBadResponse("Attributes not found", 200, ErrorCode::ERROR_READING_PARAMETERS, $app);
});

$app->post('/track_training', 'authenticate', function () use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    try {
        $training_events = $data["exercise"];
        $start_datetime = $data["start_datetime"];
        $end_datetime = $data["end_datetime"];
        $user_started_template_day_id = $data["id"];
    } catch (Exception $ex) {
        $training_events = NULL;
        $start_datetime = NULL;
        $end_datetime = NULL;
        $user_started_template_day_id = NULL;
    }
    if ($training_events != null && $start_datetime != NULL && $end_datetime != NULL && $user_started_template_day_id != NULL) {
        $db = new TemplateDb();
        $response = array();
        $response["user_temp_day"] = $db->finishTrainingDay($training_events, $start_datetime, $end_datetime, $user_started_template_day_id);
        BaseResponse::echoGoodResponse("Added training events", $response, 200, $app);
    } else
        BaseResponse::echoBadResponse("Attributes not found", 200, ErrorCode::ERROR_READING_PARAMETERS, $app);
});

$app->get('/all_started_templates', 'authenticate', function () use ($app) {
    global $user_id;
    $db = new TemplateDb();
    $data = $db->getAllStartedTemplates($user_id);
    $response = array();
    $response["all_started_templates"] = $data;
    BaseResponse::echoGoodResponse("All user started templates", $response, 200, $app);
});

function getValueFromRequest($key, $default)
{
    $app = \Slim\Slim::getInstance();
    try {
        return $app->request()->get($key);
    } catch (Exception $e) {
        return $default;
    }
}

$app->run();
?>