<?php
header('Content-Type: text/html; charset=UTF-8');

require_once $_SERVER['DOCUMENT_ROOT'] . '/database/Config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/utils/ErrorCode.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/facebook/LoginFacebook.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserDetails.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserConfig.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserSetting.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/utils/BaseResponse.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/AuthDb.php';

require $_SERVER['DOCUMENT_ROOT'] . '/libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$user_id = NULL;

function authenticate(\Slim\Route $route)
{
    $headers = apache_request_headers();
    $app = \Slim\Slim::getInstance();
    if (isset($headers['x-sport-access-token'])) {
        $db = new BaseDb();
        $token = $headers['x-sport-access-token'];
        $g_user_id = $db->getUserIdByAccessToken($token);
        if ($g_user_id == 0) {
            BaseResponse::echoBadResponse("Invalidate access token", 401, ErrorCode::INVALIDATE_ACCESS_TOKEN, $app);
            $app->stop();
        } else {
            global $user_id;
            $user_id = $g_user_id;
        }
    } else {
        BaseResponse::echoBadResponse("Did not find access token", 400, ErrorCode::ERROR_READING_PARAMETERS, $app);
        $app->stop();
    }
}

$app->post('/registration_email', function () use ($app) {
    $headers = apache_request_headers();
    if (isset($headers['x-sport-secret-token']) && $headers['x-sport-secret-token'] == SECRET_TOKEN) {
        $json = $app->request->getBody();
        $data = json_decode($json, true);
        try {
            $email = $data["email"];
            $password = $data["password"];
            $name = $data["name"];
        } catch (Exception $ex) {
            $email = NULL;
            $password = NULL;
            $name = NULL;
        }

        if ($email != NULL && $password != NULL && $name != NULL) {
            $db = new AuthDb();
            if (!$db->isEmailExist($email)) {
                $userConfig = $db->createUser($email, $password, $name);
                if ($userConfig != NULL) {
                    $response = array();
                    $user_details = $db->getUserDetails($userConfig->getId());
                    if (!$user_details->isEmpty())
                        $response["user_details"] = $user_details->getDataForApi();
                    $response["user_config"] = $userConfig->getDataForApi();
                    $response["user_setting"] = $db->getUserSetting($userConfig->getId())->getDataForApi();
                    BaseResponse::echoGoodResponse("UserDetails created", $response, 201, $app);
                } else
                    BaseResponse::echoBadResponse("Wops, something went wrong.", 200, ErrorCode::BASE_ERROR, $app);
            } else
                BaseResponse::echoBadResponse("This email already exist.", 200, ErrorCode::EMAIL_ALREADY_EXIST, $app);
        } else
            BaseResponse::echoBadResponse("Attributes not found", 200, ErrorCode::ERROR_READING_PARAMETERS, $app);
    } else
        BaseResponse::echoBadResponse("Error in registration.", 200, ErrorCode::INVALIDATE_SECRET_TOKEN, $app);
});

$app->post('/login_email', function () use ($app) {
    $headers = apache_request_headers();
    if (isset($headers['x-sport-secret-token']) && $headers['x-sport-secret-token'] == SECRET_TOKEN) {
        $json = $app->request->getBody();
        $data = json_decode($json, true);
        try {
            $email = $data["email"];
            $password = $data["password"];
        } catch (Exception $ex) {
            $email = NULL;
            $password = NULL;
        }
        if ($email != NULL && $password != NULL) {
            $db = new AuthDb();
            $user_config = $db->getUserConfigIfExist($email, $password);
            if ($user_config != NULL) {
                $response = array();
                $response["user_config"] = $user_config->getDataForApi();
                $response["user_setting"] = $db->getUserSetting($user_config->getId())->getDataForApi();
                $user_details = $db->getUserDetails($user_config->getId());
                if (!$user_details->isEmpty())
                    $response["user_details"] = $user_details->getDataForApi();
                BaseResponse::echoGoodResponse("This user found", $response, 200, $app);
                return;
            } else
                BaseResponse::echoBadResponse("Please check your email address or password.", 200, ErrorCode::INVALIDATE_EMAIL_OR_PASSWORD, $app);
        } else
            BaseResponse::echoBadResponse("Attributes not found", 200, ErrorCode::ERROR_READING_PARAMETERS, $app);
    } else
        BaseResponse::echoBadResponse("Invalidate secret token.", 200, ErrorCode::INVALIDATE_SECRET_TOKEN, $app);
});

$app->post('/login_facebook', function () use ($app) {
    $headers = apache_request_headers();
    if (isset($headers['x-sport-secret-token']) && $headers['x-sport-secret-token'] == SECRET_TOKEN) {
        $json = $app->request->getBody();
        $data = json_decode($json, true);
        try {
            $facebook_access_token = $data["facebook_access_token"];
        } catch (Exception $ex) {
            $facebook_access_token = NULL;
        }
        if ($facebook_access_token != NULL) {
            $result_facebook = LoginFacebook::getUserDataByAccessToken($facebook_access_token);
            if ($result_facebook != NULL && isset($result_facebook['id'])) {
                $facebook_id = $result_facebook['id'];
                if (isset($result_facebook['email'])) {
                    $email = $result_facebook['email'];
                } else {
                    $email = $facebook_id . "@facebook.com";
                }
                $db = new AuthDb();
                $response = array();

                if (!$db->isFacebookIdExist($facebook_id)) {
                    $user_config = $db->createUserFromFacebook($email, $facebook_id, $facebook_id, $facebook_id);
                } else {
                    $user_config = $db->getUserConfigByFacebookId($facebook_id);
                }

                if ($user_config->getStatus() == 0) {
                    $user_details = new UserDetails();
                    $user_details->parseFromFacebook($result_facebook);
                    $db->updateFbUserDetails($user_config->getId(), $user_details->getName(), $user_details->getPhotoUrl());
                }

                $user_details = $db->getUserDetails($user_config->getId());
                $response["user_details"] = $user_details->getDataForApi();
                $response["user_config"] = $user_config->getDataForApi();
                $response["user_setting"] = $db->getUserSetting($user_config->getId())->getDataForApi();
                BaseResponse::echoGoodResponse("Facebook", $response, 201, $app);
            } else
                BaseResponse::echoBadResponse("Facebook attributes not found", 200, ErrorCode::BASE_ERROR, $app);
        } else
            BaseResponse::echoBadResponse("Attributes not found", 200, ErrorCode::ERROR_READING_PARAMETERS, $app);
    } else
        BaseResponse::echoBadResponse("Secret token is wrong.", 200, ErrorCode::INVALIDATE_SECRET_TOKEN, $app);;
});

$app->post('/login_complete', 'authenticate', function () use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    $user_details = new UserDetails();
    try {
        if (isset($data["years"]))
            $user_details->setYears($data["years"]);
        if (isset($data["sex"]))
            $user_details->setSex($data["sex"]);
        if (isset($data["weight"]))
            $user_details->setWeight($data["weight"]);
        if (isset($data["height"]))
            $user_details->setHeight($data["height"]);
        if (isset($data["metric_type"]))
            $user_details->setMetricType($data["metric_type"]);
        if (isset($data["target"]))
            $user_details->setTarget($data["target"]);
    } catch (Exception $ex) {
        $user_details = NULL;
    }

    if ($user_details != NULL && !$user_details->isEmpty()) {
        global $user_id;
        $db = new AuthDb();
        if ($db->updateUserDetails($user_id, $user_details)) {
            $response = array();
            $response["user_details"] = $db->getUserDetails($user_id)->getDataForApi();
            BaseResponse::echoGoodResponse("login completed", $response, 200, $app);
        } else {
            BaseResponse::echoBadResponse("Error update a user", 200, ErrorCode::BASE_ERROR, $app);
        }
    } else
        BaseResponse::echoBadResponse("Attributes not found", 200, ErrorCode::ERROR_READING_PARAMETERS, $app);
});

$app->run();
?>