<?php
header('Content-Type: text/html; charset=UTF-8');

require_once $_SERVER['DOCUMENT_ROOT'] . '/database/BaseDb.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/Config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/utils/ErrorCode.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/utils/BaseResponse.php';
require $_SERVER['DOCUMENT_ROOT'] . '/libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$user_id = NULL;

function authenticate(\Slim\Route $route)
{
    $headers = apache_request_headers();
    $app = \Slim\Slim::getInstance();
    if (isset($headers['x-sport-access-token'])) {
        $db = new BaseDb();
        $token = $headers['x-sport-access-token'];
        $g_user_id = $db->getUserIdByAccessToken($token);
        if ($g_user_id == 0) {
            BaseResponse::echoBadResponse("Invalidate access token", 401, ErrorCode::INVALIDATE_ACCESS_TOKEN, $app);
            $app->stop();
        } else {
            global $user_id;
            $user_id = $g_user_id;
        }
    } else {
        BaseResponse::echoBadResponse("Did not find access token", 400, ErrorCode::ERROR_READING_PARAMETERS, $app);
        $app->stop();
    }
}

$app->post('/upload', 'authenticate', function () use ($app) {
    $uploads_dir = $_SERVER['DOCUMENT_ROOT'] . '/uploads';
    if (empty($_FILES)) {
        BaseResponse::echoBadResponse("File did not fined", 200, ErrorCode::FILE_DID_NOT_FINED, $app);
        return;
    }
    if (!empty($_FILES['photo']) && $_FILES['photo']['error'] !== 4) {
        if (!$_FILES['photo']['error']) {
            $temp = explode(".", $_FILES["photo"]["name"]);
            $extension = end($temp);
            $newfilename = time() . rand() . "." . $extension;
            if (!move_uploaded_file($_FILES['photo']['tmp_name'], $uploads_dir . "/" . $newfilename)) {
                BaseResponse::echoBadResponse("An error while processing the uploaded file", 200, ErrorCode::AN_ERROR_WHILE_PROCESSING_THE_UPLOADED_FILE, $app);
            } else {
                $db = new BaseDb();
                global $user_id;
                $patch = 'https://api.vvvteam.ru/uploads/' . $newfilename;
                if ($db->saveFilePatch($user_id, $patch)) {
                    $data = array();
                    $data["url"] = $patch;
                    BaseResponse::echoGoodResponse("The file was successfully uploaded to the server", $data, 201, $app);
                } else
                    BaseResponse::echoBadResponse("Error saving file to server", 200, ErrorCode::BASE_ERROR, $app);
            }
        } else
            BaseResponse::echoBadResponse("Error processing the file on the server, the possible problem the this file size", 200, ErrorCode::BASE_ERROR, $app);
    } else
        BaseResponse::echoBadResponse("Error load file", 200, ErrorCode::ERROR_LOAD_FILE, $app);
});

$app->run();
?>