<?php
header('Content-Type: text/html; charset=UTF-8');

require_once $_SERVER['DOCUMENT_ROOT'] . '/database/UserDb.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/database/Config.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/utils/ErrorCode.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserDetails.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserConfig.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/model/UserSetting.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/utils/BaseResponse.php';
require $_SERVER['DOCUMENT_ROOT'] . '/libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
$user_id = NULL;

function authenticate(\Slim\Route $route)
{
    $headers = apache_request_headers();
    $app = \Slim\Slim::getInstance();
    if (isset($headers['x-sport-access-token'])) {
        $db = new BaseDb();
        $token = $headers['x-sport-access-token'];
        $g_user_id = $db->getUserIdByAccessToken($token);
        if ($g_user_id == 0) {
            BaseResponse::echoBadResponse("Invalidate access token", 401, ErrorCode::INVALIDATE_ACCESS_TOKEN, $app);
            $app->stop();
        } else {
            global $user_id;
            $user_id = $g_user_id;
        }
    } else {
        BaseResponse::echoBadResponse("Did not find access token", 400, ErrorCode::ERROR_READING_PARAMETERS, $app);
        $app->stop();
    }
}

$app->get('/user_details', 'authenticate', function () use ($app) {
    global $user_id;
    $db = new BaseDb();
    $data = $db->getUserDetails($user_id);
    if (!$data->isEmpty()) {
        $response = array();
        $response["user_details"] = $data->getDataForApi();
        BaseResponse::echoGoodResponse("UserDetails is found", $response, 200, $app);
    } else
        BaseResponse::echoBadResponse("UserDetails is not found", 200, ErrorCode::BASE_ERROR, $app);
});

$app->get('/user_setting', 'authenticate', function () use ($app) {
    global $user_id;
    $db = new UserDb();
    $data = $db->getUserSetting($user_id);
    $response = array();
    $response["user_setting"] = $data->getDataForApi();
    BaseResponse::echoGoodResponse("UserSetting is found", $response, 200, $app);
});

$app->post('/update_user_setting', 'authenticate', function () use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    $user_setting = new UserSetting();
    try {
        if (isset($data["notification_start_training"]))
            $user_setting->setNotificationStartTraining($data["notification_start_training"]);
        if (isset($data["notification_txt"]))
            $user_setting->setNotificationTxt($data["notification_txt"]);
        if (isset($data["notification_time_start_training"]))
            $user_setting->setNotificationTimeStartTraining($data["notification_time_start_training"]);
        if (isset($data["training_time_countdown"]))
            $user_setting->setTrainingTimeCountdown($data["training_time_countdown"]);
        if (isset($data["training_time_exercise"]))
            $user_setting->setTrainingTimeExercise($data["training_time_exercise"]);
        if (isset($data["training_time_rest"]))
            $user_setting->setTrainingTimeRest($data["training_time_rest"]);
        if (isset($data["exercise_random"]))
            $user_setting->setExerciseRandom($data["exercise_random"]);
    } catch (Exception $ex) {
        $user_setting = NULL;
    }

    if ($user_setting != null) {
        global $user_id;
        $db = new UserDb();
        $db->updateUserSetting($user_id, $user_setting);
        $response = array();
        $response["user_setting"] = $user_setting->getDataForApi();
        BaseResponse::echoGoodResponse("UserSetting is updated", $response, 200, $app);
    } else  BaseResponse::echoBadResponse("UserSetting is not updated", 200, ErrorCode::BASE_ERROR, $app);
});

$app->post('/update_details', 'authenticate', function () use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    $user_details = new UserDetails();
    try {
        if (isset($data["name"]))
            $user_details->setName($data["name"]);
        if (isset($data["photo_url"]))
            $user_details->setPhotoUrl($data["photo_url"]);
        if (isset($data["years"]))
            $user_details->setYears($data["years"]);
        if (isset($data["sex"]))
            $user_details->setSex($data["sex"]);
        if (isset($data["weight"]))
            $user_details->setWeight($data["weight"]);
        if (isset($data["height"]))
            $user_details->setHeight($data["height"]);
        if (isset($data["metric_type"]))
            $user_details->setMetricType($data["metric_type"]);
        if (isset($data["target"]))
            $user_details->setTarget($data["target"]);
    } catch (Exception $ex) {
        $user_details = NULL;
    }

    if ($user_details != NULL && !$user_details->isEmpty()) {
        global $user_id;
        $db = new UserDb();
        if ($db->updateUserDetails($user_id, $user_details)) {
            $response = array();
            $response["user_details"] = $user_details->getDataForApi();
            BaseResponse::echoGoodResponse("UserDetails was updated", $response, 200, $app);
        } else {
            BaseResponse::echoBadResponse("Error update a user", 200, ErrorCode::BASE_ERROR, $app);
        }
    } else
        BaseResponse::echoBadResponse("Attributes not found", 200, ErrorCode::ERROR_READING_PARAMETERS, $app);
});

$app->run();
?>