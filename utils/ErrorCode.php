<?php

/**
 * Created by PhpStorm.
 * UserDetails: Vovnov
 * Date: 15.08.2018
 * Time: 20:33
 */
class ErrorCode
{
    Const BASE_ERROR = 999;
    Const INVALIDATE_ACCESS_TOKEN = 1000;
    Const ERROR_READING_PARAMETERS = 1001;
    Const INVALIDATE_REFRESH_TOKEN = 1002;
    Const ERROR_REFRESH_TOKEN = 1003;
    Const INVALIDATE_SECRET_TOKEN = 1004;
    Const INVALIDATE_EMAIL_OR_PASSWORD = 1005;
    Const EMAIL_ALREADY_EXIST = 1006;
    Const NAMW_ALREADY_EXIST = 1007;
    //file
    Const FILE_DID_NOT_FINED = 2001;
    Const AN_ERROR_WHILE_PROCESSING_THE_UPLOADED_FILE = 2002;
    Const ERROR_LOAD_FILE = 2003;
    //template
    Const ERROR_TEMPLATE_ALREADY_STARTED = 3001;
    Const ERROR_ALREADY_STARTED_SOME_TEMPLATE = 3002;


    private function __construct()
    {
        // throw an exception if someone can get in here (I'm paranoid)
        throw new Exception("Can't get an instance of ErrorCode");
    }
}