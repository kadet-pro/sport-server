<?php

/**
 * Created by PhpStorm.
 * UserDetails: Vovnov
 * Date: 10.10.2018
 * Time: 21:44
 */
class BaseResponse
{
    public static function echoGoodResponse($message, $response, $status_code, $app)
    {
        if ($response == NULL)
            $response = array();
        $response["error_code"] = 0;
        $response["message"] = $message;
        BaseResponse::echoResponse($status_code, $response, $app);
    }

    public static function echoBadResponse($message, $status_code, $error_code, $app)
    {
        $response = array();
        $response["error_code"] = $error_code;
        $response["message"] = $message;
        BaseResponse::echoResponse($status_code, $response, $app);
    }

    public static function echoResponse($status_code, $response, $app)
    {
        $app = \Slim\Slim::getInstance();
        $app->status($status_code);
        $app->contentType('application/json');
        echo json_encode($response,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
    }
}