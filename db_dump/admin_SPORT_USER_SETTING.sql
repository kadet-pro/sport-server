-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 185.53.168.174    Database: admin_SPORT
-- ------------------------------------------------------
-- Server version	5.5.55

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `USER_SETTING`
--

DROP TABLE IF EXISTS `USER_SETTING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_SETTING` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `NOTIFICATION_START_TRAINING` tinyint(4) NOT NULL DEFAULT '1',
  `NOTIFICATION_TXT` varchar(128) DEFAULT NULL,
  `NOTIFICATION_TIME_START_TRAINING` varchar(10) NOT NULL DEFAULT '17:00',
  `TRAINING_TIME_COUNTDOWN` int(11) NOT NULL DEFAULT '3',
  `TRAINING_TIME_EXERCISE` int(11) NOT NULL DEFAULT '30',
  `TRAINING_TIME_REST` int(11) NOT NULL DEFAULT '10',
  `EXERCISE_RANDOM` tinyint(4) NOT NULL DEFAULT '0',
  `DATE_CREATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`,`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_SETTING`
--

LOCK TABLES `USER_SETTING` WRITE;
/*!40000 ALTER TABLE `USER_SETTING` DISABLE KEYS */;
INSERT INTO `USER_SETTING` VALUES (1,22,1,NULL,'1700',3,30,10,0,'2019-02-16 17:08:11'),(2,23,0,'Hey pidar','1700',3,30,10,0,'2019-02-16 17:08:57'),(3,24,1,NULL,'1700',3,30,10,0,'2019-05-08 08:47:34'),(4,25,1,NULL,'1700',3,30,10,0,'2019-05-08 08:49:35'),(5,26,1,NULL,'1700',3,30,10,0,'2019-05-08 08:49:52'),(6,27,1,NULL,'1700',3,30,10,0,'2019-05-08 08:52:07'),(7,28,1,NULL,'1700',3,30,10,0,'2019-05-08 08:59:04'),(8,29,1,NULL,'1700',3,30,10,0,'2019-05-08 09:06:01'),(9,30,1,NULL,'1700',3,30,10,0,'2019-05-08 09:07:00'),(10,31,1,NULL,'1700',3,30,10,0,'2019-05-08 09:09:12'),(11,32,1,NULL,'1700',3,30,10,0,'2019-05-08 09:11:41'),(12,33,1,NULL,'1700',3,30,10,0,'2019-05-08 09:11:50'),(13,34,1,NULL,'1700',3,30,10,0,'2019-05-08 09:11:54'),(14,35,1,'','',3,30,10,0,'2019-05-08 09:13:29'),(15,36,1,NULL,'1700',3,30,10,0,'2019-06-01 18:55:24'),(16,37,1,NULL,'1700',3,30,10,0,'2019-06-02 16:58:41'),(17,38,1,NULL,'1700',3,30,10,0,'2019-06-02 17:00:21'),(18,39,1,NULL,'1700',3,30,10,0,'2019-06-02 17:17:22'),(19,40,1,'сержант вставай','08:00',3,30,10,0,'2019-06-02 17:20:50'),(20,41,1,NULL,'1700',3,30,10,0,'2019-06-06 07:20:14'),(21,42,1,NULL,'1700',3,30,10,0,'2019-06-06 16:58:56'),(22,43,1,'Пришло время для тренировки!','13:47',3,30,10,0,'2019-06-12 10:37:15'),(23,44,1,'','17:00',30,30,10,1,'2019-06-12 14:39:11'),(24,45,1,NULL,'17:00',3,30,10,0,'2019-06-24 07:43:46'),(25,46,1,'Пришло время для тренировки!','17:00',3,30,10,0,'2019-06-27 15:09:19'),(26,47,1,NULL,'17:00',3,30,10,0,'2019-10-22 15:26:24'),(27,48,1,NULL,'17:00',3,30,10,0,'2019-10-22 15:32:58'),(28,49,1,NULL,'17:00',3,30,10,0,'2019-11-01 07:15:11'),(29,50,1,NULL,'17:00',3,30,10,0,'2019-11-04 16:00:57'),(30,51,1,NULL,'17:00',3,30,10,0,'2019-11-04 16:05:15'),(31,52,1,NULL,'17:00',3,30,10,0,'2019-11-04 17:34:14'),(32,53,1,NULL,'17:00',3,30,10,0,'2019-11-05 13:50:40'),(33,54,1,NULL,'17:00',3,30,10,0,'2019-12-07 17:36:59'),(34,55,1,NULL,'17:00',3,30,10,0,'2019-12-18 09:54:54'),(35,56,1,NULL,'17:00',3,30,10,0,'2019-12-21 08:44:03'),(36,57,1,NULL,'17:00',3,30,10,0,'2019-12-25 11:23:08'),(37,58,1,NULL,'17:00',3,30,10,0,'2019-12-29 11:22:58'),(38,59,1,NULL,'17:00',3,30,10,0,'2019-12-31 14:57:34'),(39,60,1,NULL,'17:00',3,30,10,0,'2019-12-31 15:02:21');
/*!40000 ALTER TABLE `USER_SETTING` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-08 13:12:15
