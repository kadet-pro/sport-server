-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 185.53.168.174    Database: admin_SPORT
-- ------------------------------------------------------
-- Server version	5.5.55

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `TEMPLATE_DAY`
--

DROP TABLE IF EXISTS `TEMPLATE_DAY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `TEMPLATE_DAY` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TEMPLATE_ID` int(11) NOT NULL,
  `DAY_IN_WEEK` int(11) NOT NULL,
  `DAY_TRAINING` int(11) NOT NULL,
  `IS_REST` tinyint(4) NOT NULL,
  `DATE_CREATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TEMPLATE_DAY`
--

LOCK TABLES `TEMPLATE_DAY` WRITE;
/*!40000 ALTER TABLE `TEMPLATE_DAY` DISABLE KEYS */;
INSERT INTO `TEMPLATE_DAY` VALUES (1,1,0,1,0,'2019-06-20 18:12:12'),(2,1,0,2,0,'2019-06-20 18:19:34'),(3,1,0,3,1,'2019-07-25 20:49:35'),(4,1,0,4,1,'2019-07-25 20:49:35'),(5,1,0,5,1,'2019-07-25 20:49:35'),(6,1,0,6,1,'2019-07-25 20:49:35'),(7,1,0,7,1,'2019-07-25 20:49:35'),(8,1,0,8,1,'2019-07-25 20:49:35'),(9,1,0,9,1,'2019-07-25 20:49:35'),(10,2,1,1,1,'2019-07-27 14:56:16'),(11,2,2,2,1,'2019-07-27 14:56:16'),(12,2,3,3,1,'2019-07-27 14:56:16'),(13,2,6,4,1,'2019-07-27 14:56:16'),(16,4,0,1,0,'2019-12-09 19:45:19'),(17,4,0,2,0,'2019-12-09 19:45:19'),(18,4,0,3,0,'2019-12-09 19:45:19'),(19,4,0,4,1,'2019-12-09 19:45:19'),(20,4,0,5,0,'2019-12-09 19:45:19'),(21,4,0,6,0,'2019-12-09 19:45:19'),(22,4,0,7,0,'2019-12-09 19:45:19'),(23,4,0,8,1,'2019-12-09 19:45:19'),(24,4,0,9,0,'2019-12-09 19:45:19'),(25,4,0,10,0,'2019-12-09 19:45:19'),(26,4,0,11,0,'2019-12-09 19:45:19'),(27,4,0,12,1,'2019-12-09 19:45:19'),(28,4,0,13,0,'2019-12-09 19:45:19'),(29,4,0,14,0,'2019-12-09 19:45:19'),(30,4,0,15,0,'2019-12-09 19:45:20'),(31,4,0,16,1,'2019-12-09 19:45:20'),(32,4,0,17,0,'2019-12-09 19:45:20'),(33,4,0,18,0,'2019-12-09 19:45:20'),(34,4,0,19,0,'2019-12-09 19:45:20'),(35,4,0,20,1,'2019-12-09 19:45:20'),(36,4,0,21,0,'2019-12-09 19:45:20'),(37,4,0,22,0,'2019-12-09 19:45:20'),(38,4,0,23,0,'2019-12-09 19:45:20'),(39,4,0,24,1,'2019-12-09 19:45:20'),(40,4,0,25,0,'2019-12-09 19:45:20'),(41,4,0,26,0,'2019-12-09 19:45:20'),(42,4,0,27,0,'2019-12-09 19:45:20'),(43,4,0,28,1,'2019-12-09 19:45:20'),(44,4,0,29,0,'2019-12-09 19:45:20'),(45,4,0,30,0,'2019-12-09 19:45:20'),(47,5,0,1,0,'2020-01-07 17:10:52'),(48,5,0,2,0,'2020-01-07 17:10:53'),(49,5,0,3,0,'2020-01-07 17:10:53'),(50,5,0,4,0,'2020-01-07 17:10:53'),(51,5,0,5,0,'2020-01-07 17:10:54'),(52,5,0,6,0,'2020-01-07 17:10:54'),(53,5,0,7,0,'2020-01-07 17:10:54'),(54,5,0,8,0,'2020-01-07 17:10:54'),(55,5,0,9,0,'2020-01-07 17:10:54'),(56,5,0,10,0,'2020-01-07 17:10:54'),(57,5,0,11,0,'2020-01-07 17:10:54'),(58,5,0,12,0,'2020-01-07 17:10:54'),(59,5,0,13,0,'2020-01-07 17:10:54'),(60,5,0,14,0,'2020-01-07 17:10:54'),(61,5,0,15,0,'2020-01-07 17:10:54'),(62,5,0,16,0,'2020-01-07 17:10:54'),(63,5,0,17,0,'2020-01-07 17:10:54'),(64,5,0,18,0,'2020-01-07 17:10:54'),(65,5,0,19,0,'2020-01-07 17:10:54'),(66,5,0,20,0,'2020-01-07 17:10:54'),(67,5,0,21,0,'2020-01-07 17:10:54');
/*!40000 ALTER TABLE `TEMPLATE_DAY` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-08 13:12:10
