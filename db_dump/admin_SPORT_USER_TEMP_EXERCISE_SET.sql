-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 185.53.168.174    Database: admin_SPORT
-- ------------------------------------------------------
-- Server version	5.5.55

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `USER_TEMP_EXERCISE_SET`
--

DROP TABLE IF EXISTS `USER_TEMP_EXERCISE_SET`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER_TEMP_EXERCISE_SET` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_TEMP_EXERCISE_ID` int(11) NOT NULL,
  `TIME` int(11) NOT NULL,
  `MASS` int(11) NOT NULL,
  `REPETITIONS` int(11) NOT NULL,
  `IS_REST` int(11) NOT NULL DEFAULT '0',
  `DATE_CREATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER_TEMP_EXERCISE_SET`
--

LOCK TABLES `USER_TEMP_EXERCISE_SET` WRITE;
/*!40000 ALTER TABLE `USER_TEMP_EXERCISE_SET` DISABLE KEYS */;
INSERT INTO `USER_TEMP_EXERCISE_SET` VALUES (155,76,7,0,100,0,'2019-12-22 16:01:56'),(156,77,58,0,100,0,'2019-12-22 16:03:45'),(157,78,2,0,100,0,'2019-12-22 16:16:44'),(158,78,36,0,200,0,'2019-12-22 16:16:44'),(159,79,1,0,100,0,'2019-12-22 16:22:46'),(160,80,32,0,50,0,'2019-12-23 12:00:30'),(161,81,17,0,55,0,'2019-12-23 12:00:30'),(162,82,32,0,50,0,'2019-12-23 12:00:30'),(163,83,17,0,55,0,'2019-12-23 12:00:30'),(164,84,18,0,50,0,'2019-12-23 12:03:21'),(165,85,13,0,100,0,'2019-12-23 12:03:21'),(166,86,27,0,50,0,'2019-12-25 11:34:40'),(167,87,6,0,135,0,'2019-12-26 23:35:00'),(168,88,2,0,50,0,'2019-12-27 15:53:18'),(169,89,3,0,50,0,'2019-12-29 13:16:50'),(170,90,2,0,55,0,'2019-12-30 23:42:32'),(171,91,11,0,50,0,'2020-01-02 13:50:46'),(172,92,5,0,20,0,'2020-01-02 13:50:46'),(173,93,16,0,50,0,'2020-01-02 14:20:24'),(174,93,16,0,0,1,'2020-01-02 14:20:24'),(175,93,7,0,28,0,'2020-01-02 14:20:24'),(176,94,4,0,60,0,'2020-01-03 22:41:52'),(177,95,42,0,70,0,'2020-01-05 22:02:50'),(178,96,18,0,50,0,'2020-01-06 19:02:00'),(179,96,10,0,0,1,'2020-01-06 19:02:00'),(180,96,13,0,9,0,'2020-01-06 19:02:00'),(181,96,3,0,0,1,'2020-01-06 19:02:00'),(182,96,17,0,666,0,'2020-01-06 19:02:00'),(183,97,0,0,55,0,'2020-01-07 10:34:55'),(184,98,8,0,20,0,'2020-01-07 19:01:39'),(185,98,8,0,0,1,'2020-01-07 19:01:39'),(186,98,5,0,25,0,'2020-01-07 19:01:39'),(187,99,5,0,0,1,'2020-01-07 19:01:39'),(188,99,8,0,20,0,'2020-01-07 19:01:39'),(189,100,1,0,20,0,'2020-01-07 19:43:22'),(190,100,3,0,0,1,'2020-01-07 19:43:22'),(191,100,3,0,25,0,'2020-01-07 19:43:22'),(192,101,3,0,0,1,'2020-01-07 19:43:22'),(193,101,2,0,20,0,'2020-01-07 19:43:22'),(194,101,26,0,0,1,'2020-01-07 19:43:22'),(195,101,10,0,25,0,'2020-01-07 19:43:22'),(196,102,1098,0,0,1,'2020-01-07 19:43:22'),(197,102,5,0,20,0,'2020-01-07 19:43:22'),(198,102,6,0,0,1,'2020-01-07 19:43:22'),(199,102,4,0,25,0,'2020-01-07 19:43:22'),(200,103,1,0,0,1,'2020-01-07 19:43:22'),(201,103,7,0,20,0,'2020-01-07 19:43:22'),(202,103,2,0,0,1,'2020-01-07 19:43:22'),(203,103,28,0,25,0,'2020-01-07 19:43:22'),(204,104,3,0,0,1,'2020-01-07 19:43:22'),(205,104,17,0,20,0,'2020-01-07 19:43:22'),(206,104,2,0,0,1,'2020-01-07 19:43:22'),(207,104,1,0,25,0,'2020-01-07 19:43:22'),(208,105,1,0,0,1,'2020-01-07 19:43:22'),(209,105,3,0,20,0,'2020-01-07 19:43:22'),(210,105,1,0,0,1,'2020-01-07 19:43:22'),(211,105,2,0,25,0,'2020-01-07 19:43:22'),(212,106,1,0,0,1,'2020-01-07 19:43:22'),(213,106,5,0,20,0,'2020-01-07 19:43:22'),(214,106,2,0,0,1,'2020-01-07 19:43:22'),(215,106,2,0,25,0,'2020-01-07 19:43:22'),(216,107,2,0,20,0,'2020-01-07 20:18:23'),(217,107,1,0,0,1,'2020-01-07 20:18:23'),(218,107,2,0,25,0,'2020-01-07 20:18:23'),(219,108,3,0,0,1,'2020-01-07 20:18:23'),(220,108,1,0,20,0,'2020-01-07 20:18:23'),(221,108,3,0,25,0,'2020-01-07 20:18:23'),(222,109,2,0,0,1,'2020-01-07 20:18:23'),(223,109,5,0,20,0,'2020-01-07 20:18:23'),(224,109,6,0,0,1,'2020-01-07 20:18:23'),(225,109,4,0,25,0,'2020-01-07 20:18:23'),(226,110,9,0,0,1,'2020-01-07 20:18:23'),(227,110,12,0,20,0,'2020-01-07 20:18:23'),(228,110,13,0,0,1,'2020-01-07 20:18:23'),(229,110,6,0,25,0,'2020-01-07 20:18:23'),(230,111,1,0,0,1,'2020-01-07 20:18:23'),(231,111,4,0,20,0,'2020-01-07 20:18:23'),(232,111,6,0,0,1,'2020-01-07 20:18:23'),(233,111,34,0,25,0,'2020-01-07 20:18:23'),(234,112,10,0,0,1,'2020-01-07 20:18:23'),(235,112,13,0,20,0,'2020-01-07 20:18:23'),(236,112,8,0,0,1,'2020-01-07 20:18:23'),(237,112,23,0,25,0,'2020-01-07 20:18:23'),(238,113,3,0,0,1,'2020-01-07 20:18:23'),(239,113,6,0,20,0,'2020-01-07 20:18:23'),(240,113,1,0,0,1,'2020-01-07 20:18:23'),(241,113,6,0,25,0,'2020-01-07 20:18:23');
/*!40000 ALTER TABLE `USER_TEMP_EXERCISE_SET` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-08 13:12:28
