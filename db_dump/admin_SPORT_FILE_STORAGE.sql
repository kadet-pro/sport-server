-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 185.53.168.174    Database: admin_SPORT
-- ------------------------------------------------------
-- Server version	5.5.55

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `FILE_STORAGE`
--

DROP TABLE IF EXISTS `FILE_STORAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FILE_STORAGE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USER` int(11) NOT NULL,
  `PATCH` varchar(512) NOT NULL,
  `DATE_CREATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FILE_STORAGE`
--

LOCK TABLES `FILE_STORAGE` WRITE;
/*!40000 ALTER TABLE `FILE_STORAGE` DISABLE KEYS */;
INSERT INTO `FILE_STORAGE` VALUES (1,11,'https://api.vvvteam.ru/uploads/15322845571460728808.jpg','2018-07-22 18:35:57'),(2,10,'https://api.vvvteam.ru/uploads/15325461011797089319.jpg','2018-07-25 19:15:01'),(3,10,'https://api.vvvteam.ru/uploads/1532546120213602138.jpg','2018-07-25 19:15:20'),(4,10,'https://api.vvvteam.ru/uploads/1532546133576545962.jpg','2018-07-25 19:15:33'),(5,10,'https://api.vvvteam.ru/uploads/1532557832783917921.jpg','2018-07-25 22:30:32'),(6,10,'https://api.vvvteam.ru/uploads/1532633594431938494.jpg','2018-07-26 19:33:14'),(7,19,'https://api.vvvteam.ru/uploads/1541018908716213888.jpg','2018-10-31 20:48:28'),(8,19,'https://api.vvvteam.ru/uploads/1541019022471273719.jpg','2018-10-31 20:50:22'),(9,19,'https://api.vvvteam.ru/uploads/15410190441729328215.png','2018-10-31 20:50:44'),(10,19,'https://api.vvvteam.ru/uploads/15410191011969732266.jpg','2018-10-31 20:51:41'),(11,19,'https://api.vvvteam.ru/uploads/15410204091237074509.jpg','2018-10-31 21:13:29'),(12,36,'https://api.vvvteam.ru/uploads/15594163411093512955.jpg','2019-06-01 19:12:21'),(13,36,'https://api.vvvteam.ru/uploads/15594761451392658581.jpg','2019-06-02 11:49:05'),(14,36,'https://api.vvvteam.ru/uploads/1559476172781533696.jpg','2019-06-02 11:49:32'),(15,36,'https://api.vvvteam.ru/uploads/1559476208165970100.jpg','2019-06-02 11:50:08'),(16,36,'https://api.vvvteam.ru/uploads/15594764191934494562.jpg','2019-06-02 11:53:39'),(17,36,'https://api.vvvteam.ru/uploads/1559476689148107025.jpg','2019-06-02 11:58:09'),(18,41,'https://api.vvvteam.ru/uploads/15598082972094190007.jpg','2019-06-06 08:04:57'),(19,41,'https://api.vvvteam.ru/uploads/15598083981606335737.jpg','2019-06-06 08:06:38'),(20,41,'https://api.vvvteam.ru/uploads/1559830770538367776.jpg','2019-06-06 14:19:30'),(21,42,'https://api.vvvteam.ru/uploads/1559840397559771982.jpg','2019-06-06 16:59:57'),(22,42,'https://api.vvvteam.ru/uploads/15598405241360562553.jpg','2019-06-06 17:02:04'),(23,40,'https://api.vvvteam.ru/uploads/15607949781516864865.jpg','2019-06-17 18:09:38'),(24,45,'https://api.vvvteam.ru/uploads/1561364054973226815.jpg','2019-06-24 08:14:14'),(25,45,'https://api.vvvteam.ru/uploads/1561364080916219229.jpg','2019-06-24 08:14:40'),(26,46,'https://api.vvvteam.ru/uploads/1563468233285253584.jpg','2019-07-18 16:43:53'),(27,46,'https://api.vvvteam.ru/uploads/1570049032293594651.jpg','2019-10-02 20:43:53'),(28,46,'https://api.vvvteam.ru/uploads/15700490591711314823.jpg','2019-10-02 20:44:19'),(29,40,'https://api.vvvteam.ru/uploads/15758312181529288225.jpg','2019-12-08 18:53:38'),(30,40,'https://api.vvvteam.ru/uploads/1576244196859011114.jpg','2019-12-13 13:36:36'),(31,40,'https://api.vvvteam.ru/uploads/15762442221071442.jpg','2019-12-13 13:37:02'),(32,40,'https://api.vvvteam.ru/uploads/1576244264608460427.jpg','2019-12-13 13:37:44');
/*!40000 ALTER TABLE `FILE_STORAGE` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-08 13:12:18
