-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 185.53.168.174    Database: admin_SPORT
-- ------------------------------------------------------
-- Server version	5.5.55

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `SP_TEMPLATE_FOR_WHOM`
--

DROP TABLE IF EXISTS `SP_TEMPLATE_FOR_WHOM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `SP_TEMPLATE_FOR_WHOM` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SP_TEMPLATE_FOR_WHOM`
--

LOCK TABLES `SP_TEMPLATE_FOR_WHOM` WRITE;
/*!40000 ALTER TABLE `SP_TEMPLATE_FOR_WHOM` DISABLE KEYS */;
INSERT INTO `SP_TEMPLATE_FOR_WHOM` VALUES (1,'Мужчин'),(2,'Женщин'),(3,'Для всех'),(4,'tet job'),(5,'tet job'),(6,'tet job'),(7,'tet job'),(8,'tet job'),(9,'tet job'),(10,'tet job'),(11,'tet job'),(12,'tet job'),(13,'tet job'),(14,'tet job'),(15,'tet job'),(16,'tet job'),(17,'tet job'),(18,'tet job'),(19,'tet job'),(20,'tet job'),(21,'tet job'),(22,'tet job'),(23,'tet job'),(24,'tet job'),(25,'tet job'),(26,'tet job'),(27,'tet job');
/*!40000 ALTER TABLE `SP_TEMPLATE_FOR_WHOM` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-08 13:12:17
