-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 185.53.168.174    Database: admin_SPORT
-- ------------------------------------------------------
-- Server version	5.5.55

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `TEMPLATE`
--

DROP TABLE IF EXISTS `TEMPLATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `TEMPLATE` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(45) NOT NULL,
  `DESCRIPTION` varchar(512) NOT NULL,
  `TYPE_TRAINING_TEMPLATE` int(11) NOT NULL,
  `PHOTO_URL` varchar(1024) NOT NULL,
  `FOR_WHOM` int(11) NOT NULL,
  `GOAL` int(11) NOT NULL,
  `TRAINING_PLACE` int(11) NOT NULL,
  `LEVEL_OF_TRAINING` int(11) NOT NULL,
  `TRAINING_TIME` varchar(45) NOT NULL,
  `COUNT_TRAINING_PER_WEEK` int(11) NOT NULL,
  `CYCLE_TIME_IN_WEEK` int(11) NOT NULL,
  `ACTIVE` int(11) NOT NULL DEFAULT '1',
  `DATE_CREATE` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='TRAINING_TIME - Время тренировки\nCOUNT_TRAINING_PER_WEEK - Тренировок в неделю\nCYCLE_TIME_IN_WEEK - Длительность цикла';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TEMPLATE`
--

LOCK TABLES `TEMPLATE` WRITE;
/*!40000 ALTER TABLE `TEMPLATE` DISABLE KEYS */;
INSERT INTO `TEMPLATE` VALUES (4,'Качаем попу за 30 дней','Реально ли накачать попу всего за месяц? Ответ на этот вопрос зависит от того, что для тебя значит «накачать». Если ты думаешь, что сегодня ты могла спрятаться за фонарным столбом и единственное, что выглядывало из-за столба – это твой нос, то за 30 дней у тебя не появится попа, которую даже за камазом не спрячешь.',2,'https://avatars.mds.yandex.net/get-zen_doc/1209363/pub_5dc07899d4f07a00ae2012c6_5dc0789ce882c300ae48aca5/scale_1200',2,6,2,1,'15',0,0,1,'2019-12-09 19:35:06'),(5,'Упругая попа в домашних условиях','Это программа подготовлена для девушек которые хотят сделать упругую попу в доманих условиях.',2,'http://api.vvvteam.ru/img/template/5.jpg',2,2,2,1,'15',0,0,1,'2020-01-07 17:07:55');
/*!40000 ALTER TABLE `TEMPLATE` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-08 13:12:00
